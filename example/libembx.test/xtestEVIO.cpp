#include "EVIO.h"
#include "Tracer.h"
#include "ArgUtil.h"

using namespace libemb;
using namespace libembx;

static std::string s_command;
class MyIO:public EVIO{
public:
	virtual void onEvent(int evioEvt)
	{
		/* 必须读取出数据,否则会一直调用onEvent */
		InputGet input;
		input.waitInput();
		s_command = input.toString();
		TRACE_REL_CLASS("stdio event:%d\n",evioEvt);
	}
};

class MyTimer:public EVTimer{
public:
	virtual void onTimeout()
	{
		TRACE_REL_CLASS("timer event arrived!\n");
	}
};

void testEVIO(void)
{
	TRACE_INFO("EVIO test start >>>>>>\n");
	TRACE_INFO("q/quit: quit test.\n");
	s_command = "";
	EVLoop loop;
	loop.initialize();
	
	MyIO inWatcher;
	inWatcher.setEvent(STDIN_FILENO, EVIO_EVT_READ);
	loop.addWatcher(&inWatcher);

	MyTimer ioTimer;
	ioTimer.setTimer(1000, true);
	loop.addWatcher(&ioTimer);

	Thread loopThread;
	loopThread.start(loop);

	while(1)
	{
		if (s_command=="quit" || s_command=="q")
		{
			break;
		}
		Thread::msleep(100);
	}
	if(!loopThread.stop(1000))
	{
		loopThread.forceQuit();
	}
	TRACE_INFO("EVIO test finished <<<<<<\n");
}
