LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := test-libembx

LOCAL_CFLAGS :=
LOCAL_CXXFLAGS :=
LOCAL_LDFLAGS := 
##########################################################################
#链接库顺序:放在前面的库需要依赖于后面的库,越后的库越底层！
##########################################################################
#基础库
LOCAL_LDFLAGS +=-lembx -lemb -lpthread -lrt
#工具类开源库
#LOCAL_LDFLAGS += -lsqlite3 -ldl -ltinyxml -lcjson -lconfig++ 
LOCAL_LDFLAGS += -lev -lcjson -lconfig++ 

ifeq ($(BUILD_SYSTEM),OS_CYGWIN)
$(warning "cygwin can't use libcurl!")
else
#LOCAL_LDFLAGS += -lcurl
endif

LOCAL_LIB_PATHS :=

LOCAL_INC_PATHS := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/../../libemb \
	$(LOCAL_PATH)/../../libembx \

LOCAL_SRC_FILES := \
	xtestMain.cpp \
	xtestConfig.cpp \
	xtestEVIO.cpp \
	xtestJSON.cpp

include $(BUILD_EXECUTABLE)
