#include "Tracer.h"
#include "FileUtil.h"
#include "StrUtil.h"
#include "ArgUtil.h"
#include "yaml-cpp/yaml.h"
#include <unistd.h>
#include <iostream>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>

using namespace libemb;

int main(int argc, char* argv[])
{
	std::string argValue;
	ArgOption  argOption;
	Tracer::getInstance()->setLevel(TRACE_LEVEL_DBG);
	
	argOption.addOption("h", 0);	/* -h:帮助 */
	argOption.parseArgs(argc,argv);	/* 解析参数 */

	if(argOption.getValue("h", argValue))
	{
		FilePath filePath(argv[0]);
		TRACE_YELLOW("yamlcpp help:\n");
		TRACE_YELLOW("%s [-h]\n",CSTR(filePath.baseName()));
		TRACE_YELLOW("        -h  help\n");
		return STATUS_OK;
	}
	
    YAML::Node node = YAML::Load("[22,3,4,4]");
    cout << node[0] << endl;

	while(1)
	{
		Thread::msleep(2000);
	}
    return 0;
}
