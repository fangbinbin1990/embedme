LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := yamlcpp

LOCAL_CFLAGS :=
LOCAL_CXXFLAGS := -std=c++11
LOCAL_LDFLAGS := 
LOCAL_LIB_PATHS :=
##########################################################################
#链接库顺序:放在前面的库需要依赖于后面的库,越后的库越底层！
##########################################################################
#基础库
LOCAL_LDFLAGS +=-lemb -lyaml-cpp -lpthread -lrt

LOCAL_INC_PATHS := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/../../libemb

LOCAL_SRC_FILES := \
	yamlcpp.cpp

include $(BUILD_EXECUTABLE)
