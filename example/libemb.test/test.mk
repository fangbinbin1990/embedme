LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := test-libemb

LOCAL_CFLAGS :=
LOCAL_CXXFLAGS :=
LOCAL_LDFLAGS := 
LOCAL_LIB_PATHS :=
##########################################################################
#链接库顺序:放在前面的库需要依赖于后面的库,越后的库越底层！
##########################################################################
#基础库
LOCAL_LDFLAGS +=-lemb -lpthread -lrt

LOCAL_INC_PATHS := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/../../libemb

LOCAL_SRC_FILES := \
	testBaseType.cpp \
	testLogger.cpp \
	testMain.cpp \
	testTcp.cpp \
	testTimer.cpp \
	testUdp.cpp

include $(BUILD_EXECUTABLE)
