#include "BaseType.h"
#include "Tracer.h"
#include "Logger.h"
using namespace libemb;

void testLogger(void)
{
    TRACE_INFO("Logger test start >>>>>>\n");
	Logger& logger = Logger::getInstance();
	logger.setRoot("/tmp/log");
	logger.openLog("test");
	logger.log("test","this is a test log.\n");
	logger.closeLog("test");
    TRACE_INFO("Logger test finished <<<<<<\n");
}
