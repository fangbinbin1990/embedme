# Embedme项目简介

Embedme是一个基于linux的嵌入式应用工具类库，包括线程，线程池，Coroutine,定时器，消息队列，socket，Tuple,文件,目录，内存池，串口，CANSocket等嵌入式开发中常用的模块。Embedme集成了cJSON,sqlite,tinyxml,libconfig++,yaml-cpp,libev等优秀的开源库。它可以帮助您快速的构建稳定的嵌入式应用程序，省去广大码农造轮子的重复劳动。Embedme目前还在不断完善中，欢迎各位同行fork本开源库，也期待您的建议和开源贡献。

## 作者

有任何问题，欢迎联系： cblock@126.com 牛咕噜大人

## 注意事项

本软件遵循LGPL协议,请自觉遵守该协议,您使用本软件所引起的任何法律后果本人不承担任何责任！
如果您使用此源码,请务必保留README在您的工程代码目录下!

## 编译系统mbuild system

本工程采用自行编写的mbuild系统进行编译,mbuild系统的使用请参考:[mbuild使用说明](build/README.md)

### 目录树结构

* app: 应用程序源代码存放目录.
* build: mbuild编译系统目录.
* example: 工程示例源码目录.
* libemb: libemb库源码,此文件夹内的代码不依赖第三方开源库.
* libembx: libembx库源码,此文件夹内的代码会依赖第三方开源库.
* manual: 用户手册.
* openlibs: 目录用于放置移植好的外部库.
* opensource: 目录用于集成外部开源库.
* tools: 嵌入式常用工具.

### 编译说明

在编译前请先确认已安装autoconf,automake,libtool等工具,否则无法编译成功,如遇编译错误，请自行查看错误提示，判断是否是工具未安装。

1 . cd到工程跟目录下

```bash
$cd embedme
$ls
app build libemb libembx opensource tools
```

2 . 设置mbuild编译环境

```bash
$source build/envsetup.sh
```

3 . 设置编译目标体系

```bash
$mbuild_setup
```

4 . 编译

```bash
$mbuild_make libemb libemb
$mbuild_make example/libemb.test test
$mbuild_make app/demo demo
```

### 备注

* mbuild 命令的详细使用方法请参考[mbuild说明](build/README.md)

* 本工程支持cygwin/Android环境下编译,源码中使用OS_CYGWIN/OS_ANDROID宏来隔离代码，如果不使用宏将默认代码同时支持在Linux,cygwin,Android环境下编译.