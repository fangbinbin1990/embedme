###### 本工程参照 external/opencv ######
LOCAL_PATH := $(call my-dir)

MY_CPP_STL_INCLUDES := 	\
	$(TOP)/prebuilts/ndk/current/sources/cxx-stl/gnu-libstdc++/include \
	$(TOP)/prebuilts/ndk/current/sources/cxx-stl/gnu-libstdc++/libs/armeabi/include
MY_CPP_STL_LIBS := \
	$(TOP)/prebuilts/ndk/current/sources/cxx-stl/gnu-libstdc++/libs/armeabi/libgnustl_static.a

MY_SQLITE3_INCLUDES := \
	$(TOP)/external/sqlite/dist

#MY_CPP_CFLAGS := -DOS_ANDROID -DPRODUCT_CZT882 -frtti -fexceptions
MY_CPP_CFLAGS := -DOS_ANDROID -DPRODUCT_CZT902B -frtti -fexceptions

#############################################
######  libcjson
#############################################
include $(CLEAR_VARS)
MY_SRC_DIR := $(LOCAL_PATH)/opensource/cJSON
LOCAL_MODULE := libcjson

#LOCAL_LDLIBS += -lstdc++  ###不支持rtti和exception
LOCAL_LDFLAGS += $(MY_CPP_STL_LIBS)

LOCAL_C_INCLUDES := \
		$(MY_SRC_DIR)

LOCAL_SRC_FILES := 	\
	opensource/cJSON/cJSON.c

include $(BUILD_STATIC_LIBRARY)

#############################################
######  libconfig
#############################################
include $(CLEAR_VARS)
MY_SRC_DIR := $(LOCAL_PATH)/opensource/libconfig-1.4.9
LOCAL_MODULE := libconfig

LOCAL_CFLAGS := -DLIBCONFIG_STATIC $(MY_CPP_CFLAGS)
LOCAL_CXXFLAGS := $(LOCAL_CFLAGS)

#LOCAL_LDLIBS += -lstdc++  ###不支持rtti和exception
LOCAL_LDFLAGS += $(MY_CPP_STL_LIBS)

LOCAL_C_INCLUDES := \
	$(MY_SRC_DIR) \
	$(MY_CPP_STL_INCLUDES)

LOCAL_SRC_FILES :=	\
	opensource/libconfig-1.4.9/grammar.c \
	opensource/libconfig-1.4.9/libconfig.c \
	opensource/libconfig-1.4.9/libconfigcpp.h \
	opensource/libconfig-1.4.9/libconfigcpp.cpp \
	opensource/libconfig-1.4.9/scanctx.c \
	opensource/libconfig-1.4.9/scanner.c \
	opensource/libconfig-1.4.9/strbuf.c

include $(BUILD_STATIC_LIBRARY)


#############################################
######  libtinyalsa
#############################################
include $(CLEAR_VARS)
MY_SRC_DIR := $(LOCAL_PATH)/opensource/tinyalsa
LOCAL_MODULE := libtinyalsa

LOCAL_CFLAGS := -DLIBCONFIG_STATIC $(MY_CPP_CFLAGS)
LOCAL_CXXFLAGS := $(LOCAL_CFLAGS)

#LOCAL_LDLIBS += -lstdc++  ###不支持rtti和exception
LOCAL_LDFLAGS += $(MY_CPP_STL_LIBS)

LOCAL_C_INCLUDES := \
	$(MY_SRC_DIR) \
	$(MY_CPP_STL_INCLUDES)
	
LOCAL_SRC_FILES := \
	opensource/tinyalsa/control.c \
	opensource/tinyalsa/mixer.c \
	opensource/tinyalsa/pcm.c
include $(BUILD_STATIC_LIBRARY)

#############################################
######  libemb
#############################################
include $(CLEAR_VARS)

MY_SRC_DIR := $(LOCAL_PATH)/libemb
LOCAL_MODULE := libemb

LOCAL_CFLAGS := -DOS_UNIXLIKE $(MY_CPP_CFLAGS)
LOCAL_CXXFLAGS := $(LOCAL_CFLAGS)

#LOCAL_LDLIBS := -lstdc++  ###不支持rtti和exception
LOCAL_LDFLAGS += $(MY_CPP_STL_LIBS)
LOCAL_STATIC_LIBRARIES := libconfig libcjson
LOCAL_SHARED_LIBRARIES := libsqlite

LOCAL_C_INCLUDES := \
	$(MY_SRC_DIR)/ \
	$(MY_SRC_DIR)/../opensource/libconfig-1.4.9 \
	$(MY_SRC_DIR)/../opensource/cJSON \
	$(MY_SRC_DIR)/../opensource/tinyalsa/include/tinyalsa \
	$(MY_CPP_STL_INCLUDES) \
	$(MY_SQLITE3_INCLUDES)
	
LOCAL_SRC_FILES := \
	libemb/Array.cpp \
	libemb/CommandPipe.cpp \
	libemb/ComUtils.cpp \
	libemb/Config.cpp \
	libemb/CRCCheck.cpp \
	libemb/DateTime.cpp \
	libemb/Directory.cpp \
	libemb/Event.cpp \
	libemb/EventTimer.cpp \
	libemb/File.cpp \
	libemb/Gpio.cpp \
	libemb/GpioInput.cpp \
	libemb/HttpServer.cpp \
	libemb/IODevice.cpp \
	libemb/JSONData.cpp \
	libemb/KeyInput.cpp \
	libemb/KVProperty.cpp \
	libemb/LocalMsgQueue.cpp \
	libemb/Logger.cpp \
	libemb/MD5Check.cpp \
	libemb/Mutex.cpp \
	libemb/Network.cpp \
	libemb/RegExp.cpp \
	libemb/RemoteCommandService.cpp \
	libemb/Semaphore.cpp \
	libemb/SerialPort.cpp \
	libemb/Socket.cpp \
	libemb/SocketPair.cpp \
	libemb/SqliteWrapper.cpp \
	libemb/StateHandler.cpp \
	libemb/STPPacker.cpp \
	libemb/StringFilter.cpp \
	libemb/Thread.cpp \
	libemb/Timer.cpp \
	libemb/Tracer.cpp \
	libemb/TraceService.cpp \
	libemb/Tuple.cpp

include $(BUILD_STATIC_LIBRARY)

#############################################
######  libmedia
#############################################
include $(CLEAR_VARS)
MY_SRC_DIR := $(LOCAL_PATH)
LOCAL_MODULE := libmedia

LOCAL_CFLAGS := -DOS_UNIXLIKE $(MY_CPP_CFLAGS)
LOCAL_CXXFLAGS := $(LOCAL_CFLAGS)

#LOCAL_LDLIBS := -lstdc++ ###不支持rtti和exception
LOCAL_LDFLAGS += $(MY_CPP_STL_LIBS)
LOCAL_STATIC_LIBRARIES := libconfig libcjson libemb
LOCAL_SHARED_LIBRARIES := libsqlite

LOCAL_C_INCLUDES := \
	$(MY_SRC_DIR)/opensource/libconfig-1.4.9 \
	$(MY_SRC_DIR)/opensource/cJSON \
	$(MY_SRC_DIR)/opensource/tinyalsa/include/tinyalsa \
	$(MY_SRC_DIR)/libemb \
	$(MY_SRC_DIR)/libmedia \
	$(MY_CPP_STL_INCLUDES) \
	$(MY_SQLITE3_INCLUDES)
	
LOCAL_SRC_FILES := \
	libmedia/BmpImage.cpp \
	libmedia/FBDisplayer.cpp \
	libmedia/Image.cpp \
	libmedia/PCMFilter.cpp
	
	
include $(BUILD_STATIC_LIBRARY)
#############################################
######  libnet
#############################################
include $(CLEAR_VARS)
MY_SRC_DIR := $(LOCAL_PATH)
LOCAL_MODULE := libnet

LOCAL_CFLAGS := -DOS_UNIXLIKE $(MY_CPP_CFLAGS)
LOCAL_CXXFLAGS := $(LOCAL_CFLAGS)

#LOCAL_LDLIBS := -lstdc++ ###不支持rtti和exception
LOCAL_LDFLAGS += $(MY_CPP_STL_LIBS)
LOCAL_STATIC_LIBRARIES := libconfig libcjson libemb
LOCAL_SHARED_LIBRARIES := libsqlite

LOCAL_C_INCLUDES := \
	$(MY_SRC_DIR)/opensource/libconfig-1.4.9 \
	$(MY_SRC_DIR)/opensource/cJSON \
	$(MY_SRC_DIR)/libemb \
	$(MY_SRC_DIR)/libnet \
	$(MY_CPP_STL_INCLUDES) \
	$(MY_SQLITE3_INCLUDES)
	
LOCAL_SRC_FILES := \
	libnet/U8300NdisService.cpp
	
include $(BUILD_STATIC_LIBRARY)

#############################################
######  appaudio
#############################################
include $(CLEAR_VARS)
MY_SRC_DIR := $(LOCAL_PATH)
LOCAL_MODULE := libappaudio

LOCAL_CFLAGS := -DOS_UNIXLIKE $(MY_CPP_CFLAGS)
LOCAL_CXXFLAGS := $(LOCAL_CFLAGS)

#LOCAL_LDLIBS := -lstdc++ ###不支持rtti和exception
LOCAL_LDFLAGS += $(MY_CPP_STL_LIBS)
LOCAL_STATIC_LIBRARIES := libconfig libcjson libemb
LOCAL_SHARED_LIBRARIES := libsqlite

LOCAL_C_INCLUDES := \
	$(MY_SRC_DIR)/opensource/libconfig-1.4.9 \
	$(MY_SRC_DIR)/opensource/cJSON \
	$(MY_SRC_DIR)/opensource/tinyalsa/include/tinyalsa \
	$(MY_SRC_DIR)/libemb \
	$(MY_SRC_DIR)/libmedia \
	$(MY_SRC_DIR)/appaudio \
	$(MY_CPP_STL_INCLUDES) \
	$(MY_SQLITE3_INCLUDES)
	
LOCAL_SRC_FILES := \
	appaudio/Audio4CZT882.cpp \
	appaudio/Audio4CZT902A.cpp \
	appaudio/Audio4CZT902B.cpp \
	appaudio/Audio4GDT811.cpp \
	appaudio/AudioPolicy.cpp
	
include $(BUILD_STATIC_LIBRARY)
#############################################
######  appradio
#############################################
include $(CLEAR_VARS)
MY_SRC_DIR := $(LOCAL_PATH)
LOCAL_MODULE := libappradio

LOCAL_CFLAGS := -DOS_UNIXLIKE $(MY_CPP_CFLAGS)
LOCAL_CXXFLAGS := $(LOCAL_CFLAGS)

#LOCAL_LDLIBS := -lstdc++ ###不支持rtti和exception
LOCAL_LDFLAGS += $(MY_CPP_STL_LIBS)
LOCAL_STATIC_LIBRARIES := libconfig libcjson libtinyalsa libemb
LOCAL_SHARED_LIBRARIES := libsqlite

LOCAL_C_INCLUDES := \
	$(MY_SRC_DIR)/opensource/libconfig-1.4.9 \
	$(MY_SRC_DIR)/opensource/cJSON \
	$(MY_SRC_DIR)/opensource/tinyalsa/include/tinyalsa \
	$(MY_SRC_DIR)/libemb \
	$(MY_SRC_DIR)/libmedia \
	$(MY_SRC_DIR)/appaudio \
	$(MY_SRC_DIR)/appradio \
	$(MY_CPP_STL_INCLUDES) \
	$(MY_SQLITE3_INCLUDES)
	
LOCAL_SRC_FILES := \
	appradio/AtParser.cpp \
	appradio/BaseDevice.cpp \
	appradio/DeviceManager.cpp \
	appradio/DmrModem.cpp \
	appradio/HandsetSM19A2.cpp \
	appradio/Modem.cpp \
	appradio/ModemFactory.cpp \
	appradio/ModemU8300.cpp \
	appradio/TetraModem.cpp \
	appradio/TetraUtils.cpp
	
include $(BUILD_STATIC_LIBRARY)

#############################################
######  rcs-boost
#############################################
include $(CLEAR_VARS)
MY_SRC_DIR := $(LOCAL_PATH)/app
LOCAL_MODULE := rcs-boost

LOCAL_CFLAGS += $(MY_CPP_CFLAGS)
LOCAL_CXXFLAGS := $(LOCAL_CFLAGS)
#LOCAL_LDFLAGS += $(MY_CPP_STL_LIBS)	##在native_app.mk中已经定义

LOCAL_STATIC_LIBRARIES := libnet libappradio libappaudio libmedia libemb libconfig libcjson
LOCAL_SHARED_LIBRARIES := libsqlite

LOCAL_C_INCLUDES := \
	$(MY_SRC_DIR)/ \
	$(MY_SRC_DIR)/../opensource/libconfig-1.4.9 \
	$(MY_SRC_DIR)/../opensource/cJSON \
	$(MY_SRC_DIR)/../opensource/tinyalsa/include/tinyalsa \
	$(MY_SRC_DIR)/../libemb \
	$(MY_SRC_DIR)/../libmedia \
	$(MY_SRC_DIR)/../libnet \
	$(MY_SRC_DIR)/../appaudio \
	$(MY_SRC_DIR)/../appradio \
	$(MY_SQLITE3_INCLUDES)

LOCAL_SRC_FILES := \
	app/AppConfig.cpp \
	app/rcs-boost.cpp

include $(BUILD_NATIVE_APP)

#############################################
######  rcs-czt882
#############################################
include $(CLEAR_VARS)
MY_SRC_DIR := $(LOCAL_PATH)/app
LOCAL_MODULE := rcs-czt882

LOCAL_CFLAGS += $(MY_CPP_CFLAGS)
LOCAL_CXXFLAGS := $(LOCAL_CFLAGS)
#LOCAL_LDFLAGS += $(MY_CPP_STL_LIBS)	##在native_app.mk中已经定义

LOCAL_STATIC_LIBRARIES := libnet libappradio libappaudio libmedia libemb libconfig libcjson
LOCAL_SHARED_LIBRARIES := libsqlite

LOCAL_C_INCLUDES := \
	$(MY_SRC_DIR)/ \
	$(MY_SRC_DIR)/../opensource/libconfig-1.4.9 \
	$(MY_SRC_DIR)/../opensource/cJSON \
	$(MY_SRC_DIR)/../opensource/tinyalsa/include/tinyalsa \
	$(MY_SRC_DIR)/../libemb \
	$(MY_SRC_DIR)/../libmedia \
	$(MY_SRC_DIR)/../libnet \
	$(MY_SRC_DIR)/../appaudio \
	$(MY_SRC_DIR)/../appradio \
	$(MY_SQLITE3_INCLUDES)

LOCAL_SRC_FILES := \
	app/AppConfig.cpp \
	app/czt882/czt882.cpp \
	app/czt882/RemoteControlService.cpp

include $(BUILD_NATIVE_APP)

#############################################
######  czt882lte
#############################################
include $(CLEAR_VARS)
MY_SRC_DIR := $(LOCAL_PATH)/app
LOCAL_MODULE := czt882lte

LOCAL_CFLAGS += $(MY_CPP_CFLAGS)
LOCAL_CXXFLAGS := $(LOCAL_CFLAGS)
#LOCAL_LDFLAGS += $(MY_CPP_STL_LIBS)	##在native_app.mk中已经定义

LOCAL_STATIC_LIBRARIES := libnet libappradio libappaudio libmedia libemb libconfig libcjson
LOCAL_SHARED_LIBRARIES := libsqlite

LOCAL_C_INCLUDES := \
	$(MY_SRC_DIR)/ \
	$(MY_SRC_DIR)/../opensource/libconfig-1.4.9 \
	$(MY_SRC_DIR)/../opensource/cJSON \
	$(MY_SRC_DIR)/../opensource/tinyalsa/include/tinyalsa \
	$(MY_SRC_DIR)/../libemb \
	$(MY_SRC_DIR)/../libmedia \
	$(MY_SRC_DIR)/../libnet \
	$(MY_SRC_DIR)/../appaudio \
	$(MY_SRC_DIR)/../appradio \
	$(MY_SRC_DIR)/../app/czt902bNJYG \
	$(MY_SQLITE3_INCLUDES)

LOCAL_SRC_FILES := \
	app/AppConfig.cpp \
	app/czt882lte/czt882lte.cpp \
	app/czt902bNJYG/NJYGDTService.cpp \
	app/czt902bNJYG/NJYGPacker.cpp

include $(BUILD_NATIVE_APP)

#############################################
######  czt902bNJYG 南京有轨
#############################################
include $(CLEAR_VARS)
MY_SRC_DIR := $(LOCAL_PATH)/app
LOCAL_MODULE := czt902bNJYG

LOCAL_CFLAGS += $(MY_CPP_CFLAGS)
LOCAL_CXXFLAGS := $(LOCAL_CFLAGS)
#LOCAL_LDFLAGS += $(MY_CPP_STL_LIBS)	##在native_app.mk中已经定义

LOCAL_STATIC_LIBRARIES := libnet libappradio libappaudio libmedia libemb libconfig libcjson
LOCAL_SHARED_LIBRARIES := libsqlite

LOCAL_C_INCLUDES := \
	$(MY_SRC_DIR)/ \
	$(MY_SRC_DIR)/../opensource/libconfig-1.4.9 \
	$(MY_SRC_DIR)/../opensource/cJSON \
	$(MY_SRC_DIR)/../opensource/tinyalsa/include/tinyalsa \
	$(MY_SRC_DIR)/../libemb \
	$(MY_SRC_DIR)/../libmedia \
	$(MY_SRC_DIR)/../libnet \
	$(MY_SRC_DIR)/../appaudio \
	$(MY_SRC_DIR)/../appradio \
	$(MY_SRC_DIR)/../app/czt902bNJYG \
	$(MY_SQLITE3_INCLUDES)

LOCAL_SRC_FILES := \
	app/AppConfig.cpp \
	app/czt902bNJYG/czt902bNJYG.cpp \
	app/czt902bNJYG/NJYGDTService.cpp \
	app/czt902bNJYG/NJYGPacker.cpp

include $(BUILD_NATIVE_APP)


#############################################
######  czt902b
#############################################
include $(CLEAR_VARS)
MY_SRC_DIR := $(LOCAL_PATH)/app
LOCAL_MODULE := czt902b

LOCAL_CFLAGS += $(MY_CPP_CFLAGS)
LOCAL_CXXFLAGS := $(LOCAL_CFLAGS)
#LOCAL_LDFLAGS += $(MY_CPP_STL_LIBS)	##在native_app.mk中已经定义

LOCAL_STATIC_LIBRARIES := libnet libappradio libappaudio libmedia libemb libconfig libcjson
LOCAL_SHARED_LIBRARIES := libsqlite

LOCAL_C_INCLUDES := \
	$(MY_SRC_DIR)/ \
	$(MY_SRC_DIR)/../opensource/libconfig-1.4.9 \
	$(MY_SRC_DIR)/../opensource/cJSON \
	$(MY_SRC_DIR)/../opensource/tinyalsa/include/tinyalsa \
	$(MY_SRC_DIR)/../libemb \
	$(MY_SRC_DIR)/../libmedia \
	$(MY_SRC_DIR)/../libnet \
	$(MY_SRC_DIR)/../appaudio \
	$(MY_SRC_DIR)/../appradio \
	$(MY_SQLITE3_INCLUDES)

LOCAL_SRC_FILES := \
	app/AppConfig.cpp \
	app/czt902b/czt902b.cpp

include $(BUILD_NATIVE_APP)

#############################################
######  tracerclient
#############################################
include $(CLEAR_VARS)
MY_SRC_DIR := $(LOCAL_PATH)/app
LOCAL_MODULE := tracerclient

LOCAL_CFLAGS += $(MY_CPP_CFLAGS)
LOCAL_CXXFLAGS := $(LOCAL_CFLAGS)
#LOCAL_LDFLAGS += $(MY_CPP_STL_LIBS)	##在native_app.mk中已经定义

LOCAL_STATIC_LIBRARIES := libnet libemb libconfig libcjson

LOCAL_C_INCLUDES := \
	$(MY_SRC_DIR)/ \
	$(MY_SRC_DIR)/../opensource/libconfig-1.4.9 \
	$(MY_SRC_DIR)/../opensource/cJSON \
	$(MY_SRC_DIR)/../opensource/tinyalsa/include/tinyalsa \
	$(MY_SRC_DIR)/../libemb

LOCAL_SRC_FILES := \
	app/tracerclient.cpp

include $(BUILD_NATIVE_APP)

#############################################
######  test
#############################################
include $(CLEAR_VARS)
MY_SRC_DIR := $(LOCAL_PATH)/test
LOCAL_MODULE := rcs-test

LOCAL_CFLAGS += $(MY_CPP_CFLAGS)
LOCAL_CXXFLAGS := $(LOCAL_CFLAGS)
#LOCAL_LDFLAGS += $(MY_CPP_STL_LIBS)	##在native_app.mk中已经定义

LOCAL_STATIC_LIBRARIES := libnet libappradio libappaudio libmedia libemb libconfig libcjson libtinyalsa
LOCAL_SHARED_LIBRARIES := libsqlite

LOCAL_C_INCLUDES := \
	$(MY_SRC_DIR)/ \
	$(MY_SRC_DIR)/../opensource/libconfig-1.4.9 \
	$(MY_SRC_DIR)/../opensource/cJSON \
	$(MY_SRC_DIR)/../opensource/tinyalsa/include/tinyalsa \
	$(MY_SRC_DIR)/../libemb \
	$(MY_SRC_DIR)/../libmedia \
	$(MY_SRC_DIR)/../libnet \
	$(MY_SRC_DIR)/../appaudio \
	$(MY_SRC_DIR)/../appradio \
	$(MY_SRC_DIR)/../app \
	$(MY_SQLITE3_INCLUDES)

LOCAL_SRC_FILES := \
	test/test.cpp \
	test/TestAny.cpp \
	test/TestArray.cpp \
	test/TestAudioCodec.cpp \
	test/TestAudioPolicy.cpp \
	test/TestCommandPipe.cpp \
	test/TestCom.cpp \
	test/TestConfig.cpp \
	test/TestDateTime.cpp \
	test/TestDirectory.cpp \
	test/TestEvent.cpp \
	test/TestGpio.cpp \
	test/TestHttp.cpp \
	test/TestImage.cpp \
	test/TestInputEvent.cpp \
	test/TestJSON.cpp \
	test/TestKVProperty.cpp \
	test/TestLogger.cpp \
	test/TestMain.cpp \
	test/TestMsgQueue.cpp \
	test/TestMT680.cpp \
	test/TestNetwork.cpp \
	test/TestOSSAudio.cpp \
	test/TestRegExp.cpp \
	test/TestSocket.cpp \
	test/TestSTPPacker.cpp \
	test/TestStringFilter.cpp \
	test/TestThread.cpp \
	test/TestTimer.cpp \
	test/TestTracer.cpp \
	test/TestU8300.cpp \
	test/TestUtils.cpp

include $(BUILD_NATIVE_APP)

#############################################
######  配置文件预置
#############################################
$(warning force copy files:$(LOCAL_MODULE_PATH))
$(shell chmod 777 $(LOCAL_PATH)/app/android.rcs.conf)
$(shell chmod 777 $(LOCAL_PATH)/app/android.startrcs)
$(shell chmod 777 $(LOCAL_PATH)/test/test.conf)
$(shell cp -rf $(LOCAL_PATH)/app/android.rcs.conf $(LOCAL_MODULE_PATH)/rcs.conf)
$(shell cp -rf $(LOCAL_PATH)/app/android.startrcs $(LOCAL_MODULE_PATH)/startrcs)

