# mbuild编译系统说明

build目录是mbuild编译系统的源码，build/envsetup.sh是编译环境设置脚本。在使用mbuild系统前，必须在工程根目录下执行：

```bash
$source ./build/envsetup
```

执行完成后将可使用mbuild系统提供的系列命令mbuild_xxx。

## 命令说明

```bash
$mbuild_setup #编译环境设置(默认为x86配置,如果需要交叉编译,可以修改profile.sh文件,或选择msetup菜单中的specify by myself选项设置)
$mbuild_make <directory> <target> <linktype> #编译directory目录下的target目标
$mbuild_clean <directory> <target> #清理directory目录下的target目标
$mbuild_remake <directory> <target> <linktype> # 重新编译directory目录下的target目标
$mbuild_project <directory> <target> [exec|lib] #在directory目录下创建target目标的子工程
$mbuild_auto #自动编译(自动编译脚本:autobuild.sh)
```

使用mbuild命令必须指定两个参数<directory\> <target\>

* <directory\>  源码目录
* <target\> &ensp;&ensp;&ensp;源码目录下的目标名称，用户需编写对应的目标构建文件<target\>.mk
* <linktype\> &ensp;指定编译链接库的类型,static或者shared

## 目标构建文件target.mk说明

mbuild系统支持构建以下几种目标

* 可执行文件(BUILD\_EXECUTABLE)
* 编译链接库(BUILD\_LIBRARY)
* 编译QT项目(BUILD\_QTPROJECT)
* 部署文件(BUILD\_PREBUILD)

注意：每个target.mk文件仅支持编译一个目标,每个目标需对应一个mk文件。

### target.mk模板1(编译目标)

```makefile
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE :=
LOCAL_CFLAGS :=
LOCAL_CXXFLAGS :=
LOCAL_LDFLAGS :=
LOCAL_LIB_PATHS :=
LOCAL_INC_PATHS := $(LOCAL_PATH)
LOCAL_SRC_FILES :=
LOCAL_LIB_TYPE := static
include $(BUILD_LIBRARY)
```

### target.mk模板2(部署文件)

```makefile
include $(CLEAR_VARS)
LOCAL_MODULE := config
LOCAL_PREBUILD_SRC_FILES:= app.conf
LOCAL_PREBUILD_DST_PATH := 
include $(BUILD_PREBUILD)
```

## 常用变量说明

```makefile
LOCAL_PATH       #当前源码目录,固定写为LOCAL_PATH := $(call my-dir) 
CLEAR_VARS       #清除编译变量
LOCAL_MODULE     #模块名称，最终生成的目标名称
LOCAL_CFLAGS     #编译参数CFLAGS
LOCAL_LDFLAGS    #链接参数LDFLAGS
LOCAL_INC_PATHS  #头文件搜索路径
LOCAL_LIB_PATHS  #链接库搜索路径
LOCAL_LIB_TYPE   #目标库的类型：static或者shared
LOCAL_MODULE     #模块名称，最终生成的目标名称
LOCAL_PREBUILD_SRC_FILES #部署源文件
LOCAL_PREBUILD_DST_PATH  #部署文件的目标路径，默认部署到output/bin目录下
GCC #指定c编译器,如果不指定,则由mbuild系统确定编译器
CC  #指定c++编译器,如果不指定,则由mbuild系统确定编译器
AR  #指定链接器,如果不指定,则由mbuild系统确定链接器
```

## 工程样例

假定在工程目录下有hello目录，目录下有源码hello.h，hello.c，main.c。我们的目标是生成一个libhello.a和一个可执行程序hello.

1 . 在hello目录下新建libhello.a目标的构建文件libhello.mk:

```makefile
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := libhello
LOCAL_CFLAGS :=
LOCAL_CXXFLAGS :=
LOCAL_LDFLAGS :=
LOCAL_LIB_PATHS :=
LOCAL_INC_PATHS := $(LOCAL_PATH)
LOCAL_SRC_FILES := hello.c
LOCAL_LIB_TYPE := static
include $(BUILD_LIBRARY)
```

2 . 在hello目录下新建hello目标的构建文件hello.mk:

```makefile
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := hello
LOCAL_CFLAGS :=
LOCAL_LDFLAGS := -lpthread
LOCAL_INC_PATHS := $(LOCAL_PATH)
LOCAL_SRC_FILES := main.cpp hello.c
include $(BUILD_EXECUTABLE)
```

3 . 设置编译环境:

```bash
$source ./build/envsetup.sh
$mbuild_setup
```

4 . 编译目标:

```bash
    $mbuild_make hello libhello static
    $mbuild_make hello hello
```

5 . 在output目录的lib和bin文件夹下你将发现编译出的目标文件:

```bash
    $ls output-x86_64/bin
    hello
    $ls output-x86_64/lib
    libhello.a
```
