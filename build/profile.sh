#!/bin/sh
###############################################################################
##  Author: cblock@126.com  FergusZeng 2016 ShenZhen,China 
##  本文件为工程自定义脚本,可按需求来自定义工程类型。
##  祝使用愉快！
###############################################################################

:<< EOF # 这里开始是注释~~~
sethost 主要设置三个变量:
HOST       --- 目标程序运行的主机,默认为当前主机,也用于决定是否使用CROSS_COMPILE
HOST_NAME   --- 作为输出目录output的后缀,也用于区分openlibs目录下的库文件路径，以及qt项目的编译目标目录
HOST_FLAGS --- 特定编译器的标志,会自动添加到CC_FLAGS中去
EOF
function sethost()
{
	echo -e "\033[33m\033[1m"
	echo -e "----------host list-----------"
	echo -e " 0: x86 host (default)"
	echo -e " 1: arm-linux-gnueabihf"
	echo -e "99: specify by myself"
	echo -e "----------------------------------"
	echo -e "input you selection:\033[0m"
	read n
    case $n in
	"1") 
		HOST=arm-linux-gnueabihf
		HOST_FNAME=$HOST
		HOST_FLAGS="-Wno-deprecated"
		;;
	"99")
	    while true
	    do
    		echo -e "please input host:"
    		read host
    		echo -e "please input host alias for output suffix:"
    		read hostout
    		echo -e "you specify the host: $host, host alias:$hostout. Are you sure?(Y/n)"
    		read sure
    		if [ $sure = "Y" -o  $sure = "y" ];then
    		    HOST=$host
    		    HOST_FNAME=$hostout
    		    break
            else
                echo -e "\033[31m\033[1myou must specify a host for product:$PRODUCT_NAME\033[0m"    		
    		fi
	    done
		;;
	*)
		echo -e "\033[33m\033[1mload default x86 host.\033[0m"
		;;
	esac
}

:<< EOF # 这里开始是注释~~~
 setproduct 用于定义PRODUCT_NAME变量
 PRODUCT_NAME --- 在源码中可以使用此处定义的产品名称作为宏定义，达到隔离代码的目的。
                  比如这里定义PRODUCT_NAME=foo,则代码中可以使用
                  #ifdef PRODUCT_FOO
                  ...
                  #else
                  ...
                  #endif
注意:脚本中PRODUCT_NAME变量大小写都可以，最后Makefile会把它转换成大写，代码中也必须写成大写.
    app目录下一般放置某个产品的某个应用程序，因此app下的目录名称建议定义为PRODUCT_NAME.APP_NAME
	比如产品名为default,app名称为demo,我们把app的main函数所在的源文件目录命名为default.demo
EOF
function setproduct()
{
	echo -e "\033[33m\033[1m"
    echo -e "----------product list-----------"
    echo -e " 0: default (not set)"
    echo -e "----------------------------------"
    echo -e "input you selection:\033[0m"
    read n
    case $n in
	"1")
		PRODUCT_NAME=foo
		;;
	*)
		PRODUCT_NAME=default
		;;
	esac
}
