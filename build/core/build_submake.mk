##############################################################
##  Author: cblock@126.com  FergusZeng 2016 ShenZhen,China  ##
##############################################################
#                     target.mk template
##############################################################
# LOCAL_PATH := $(call my-dir)
# include $(CLEAR_VARS)
#
# LOCAL_LIB_TYPE:=static
#
# LOCAL_SUB_DIR:=source_dir
# SUB_MAKEFILE_PATH:=Makefile.mbuild
#
# include $(BUILD_SUBMAKE)
##############################################################

ifeq ($(BUILD_SYSTEM),)
$(error "BUILD_SYSTEM is null.")
endif

ifeq ($(BUILD_OUTPUT_PATH),)
$(error "BUILD_OUTPUT_PATH is null.")
endif

ifeq ($(LOCAL_LIB_TYPE),)
$(error "LOCAL_LIB_TYPE is null.")
endif

ifeq ($(LOCAL_SUB_DIR),)
$(error "LOCAL_SUB_DIR is null.")
endif

ifeq ($(SUB_MAKEFILE_PATH),)
$(error "SUB_MAKEFILE_PATH is null.")
endif

#如果设置了LINK_TYPE,则覆盖mk中的LOCAL_LIB_TYPE
ifneq ($(LINK_TYPE),)
LOCAL_LIB_TYPE := $(LINK_TYPE)
endif

ifeq ($(LOCAL_LIB_TYPE),shared)
$(info "build shared lib.")
else ifeq ($(LOCAL_LIB_TYPE),static)
$(info "build static lib.")
else
$(error "LOCAL_LIB_TYPE:$(LOCAL_LIB_TYPE) error!")
endif

ifneq ($(SUB_CONFIGURE_SCRIPT),)
PRE_MAKE=$(SUB_CONFIGURE_SCRIPT)
else
PRE_MAKE=echo ""
endif

.PHONY:all
all:
	cd $(LOCAL_PATH)/$(LOCAL_SUB_DIR);$(PRE_MAKE);make all -f $(SUB_MAKEFILE_PATH) HOST=$(HOST) LINKTYPE=$(LOCAL_LIB_TYPE) PREFIX=$(BUILD_OUTPUT_PATH) PROJECT_ROOT=$(PROJECT_ROOT)
	cd $(LOCAL_PATH)/$(LOCAL_SUB_DIR);make install -f $(SUB_MAKEFILE_PATH) HOST=$(HOST) LINKTYPE=$(LOCAL_LIB_TYPE) PREFIX=$(BUILD_OUTPUT_PATH) PROJECT_ROOT=$(PROJECT_ROOT)
.PHONY: clean
clean:
	cd $(LOCAL_PATH)/$(LOCAL_SUB_DIR);make clean -f $(SUB_MAKEFILE_PATH) HOST=$(HOST) LINKTYPE=$(LOCAL_LIB_TYPE) PREFIX=$(BUILD_OUTPUT_PATH) PROJECT_ROOT=$(PROJECT_ROOT)