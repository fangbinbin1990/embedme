##############################################################
##  Author: cblock@126.com  FergusZeng 2016 ShenZhen,China  ##
##############################################################
#                     target.mk template
##############################################################
# LOCAL_PATH := $(call my-dir)
# include $(CLEAR_VARS)
#
# LOCAL_SUB_DIR:=source_dir
# SUB_CONFIGURE_SCRIPT:=./configure --prefix=$(LOCAL_OUTPUT_PATH) --host=$(HOST)
#
# include $(BUILD_SUBCONF)
##############################################################

ifeq ($(BUILD_SYSTEM),)
$(error "BUILD_SYSTEM is null.")
endif

ifeq ($(BUILD_OUTPUT_PATH),)
$(error "BUILD_OUTPUT_PATH is null.")
endif

ifeq ($(LOCAL_SUB_DIR),)
$(error "LOCAL_SUB_DIR is null.")
endif

ifeq ($(SUB_CONFIGURE_SCRIPT),)
$(error "SUB_CONFIGURE_SCRIPT is null.")
endif

.PHONY:all
all:
	cd $(LOCAL_PATH);if [ -e $(LOCAL_SUB_DIR).tar.gz ];then \
	rm -rf $(LOCAL_SUB_DIR);tar zxvf $(LOCAL_SUB_DIR).tar.gz;elif [ -e $(LOCAL_SUB_DIR).tar.bz2 ];then \
	rm -rf $(LOCAL_SUB_DIR);tar jxvf $(LOCAL_SUB_DIR).tar.bz2;else \
	echo "no source tar in $(LOCAL_PATH)";exit -1;fi;
	cd $(LOCAL_PATH)/$(LOCAL_SUB_DIR);$(SUB_CONFIGURE_SCRIPT);make && make install
.PHONY: clean
clean:
	cd $(LOCAL_PATH);rm -rf $(LOCAL_SUB_DIR);