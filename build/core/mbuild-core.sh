#!/bin/sh
##############################################################
##  Author: cblock@126.com  FergusZeng 2016 ShenZhen,China  ##
##  WARNING:本文件为mbuild系统核心脚本文件，修改时务必谨慎! ##
##############################################################
CPU_NUM=4
HOST=
HOST_FLAGS=
HOST_FNAME=`uname -p`
PRODUCT_NAME=default
BUILD_SYSTEM=`uname -o | tr '[A-Z]' 'a-z'`;
if [[ $BUILD_SYSTEM =~ "cygwin" ]];then
	BUILD_SYSTEM=OS_CYGWIN
else
	BUILD_SYSTEM=OS_UNIX
fi

function mbuild_make()
{
    if [ $# -lt 2 -o $# -gt 3 ];then
        echo "[usage] mbuild_make <directory> <target> <libtype>"
	    return
	elif [ $# = 3 ];then
	    LINK_TYPE=$3
	else
	    LINK_TYPE=
    fi
	PNAME="$(echo $PRODUCT_NAME | tr '[:lower:]' '[:upper:]')"
	if [ "$PNAME" == "DEFAULT" ];then
		echo -e "\033[33m\033[1mbuild_make $1 $2 for HOST: $HOST PRODUCT: not set\033[0m"
	else
    	echo -e "\033[33m\033[1mbuild_make $1 $2 for HOST: $HOST PRODUCT: PRODUCT_$PNAME\033[0m"
	fi
    make all -f build/core/main.mk -j$CPU_NUM SOURCES_DIR=$1 HOST_FLAGS="$HOST_FLAGS" TARGET_NAME=$2 LINK_TYPE=$LINK_TYPE PROJECT_ROOT=$PROJECT_ROOT HOST=$HOST HOST_FNAME=$HOST_FNAME PRODUCT_NAME=$PNAME
	if [ "$?" -ne 0 ]; then
    	while true
    	do
    	echo -e "\033[31m\033[1mError occur on mbuild_make $1 $2 , press Ctrl+C to quit!\033[0m";
    	read n
    	done	
    	#echo -e "\033[31m\033[1mError occur on mbuild_make $1 $2 !\033[0m";
    	#echo "Error occur on mbuild_make $1 $2 !" >> $PROJECT_ROOT/error.txt
    fi
}

# 创建子工程模块
function mbuild_project()
{
    if [ $# -ne 3 ];then
        echo "[usage] mbuild_project <directory> <target> [exec|lib]"
	    return
	else
		DIR=$1
		PROJ=$2
	    TYPE=$3
    fi
	# 检查参数
	if [ "$TYPE" == "exec" -a "$TYPE" == "lib" ];then
		echo "[usage] mbuild_project <directory> <target> [exec|lib]"
	    return
	fi
	# 工程已经存在,则返回
	if [ -f $DIR/$PROJ.mk ];then
		echo "project $DIR/$PROJ.mk allready exist!"
		return
	fi
    # 目录不存在则创建
	if [ ! -d $DIR ];then
		mkdir -p $DIR
	fi
	# 拷贝工程模板
	if [ "$TYPE" == "exec" ];then
        cp build/templates/target-exec.mk.in $DIR/$PROJ.mk
	elif [ "$TYPE" == "lib" ];then
    	cp build/templates/target-lib.mk.in $DIR/$PROJ.mk
	else
		echo "[usage] mbuild_project <directory> <target> [exec|lib]"
	    return
	fi
	# 编辑模板
	LINE_NUM=`sed -n '/LOCAL_MODULE/=' $DIR/$PROJ.mk`
	sed -i "$LINE_NUM""c\LOCAL_MODULE := $PROJ" $DIR/$PROJ.mk
	echo "create project $DIR/$PROJ.mk ok."
}

function mbuild_clean()
{
	if [ $# -lt 2 -o $# -gt 3 ];then
		echo "[usage] mbuild_clean <directory> <target>"
		return
	fi
	PNAME="$(echo $PRODUCT_NAME | tr '[:lower:]' '[:upper:]')"
	if [ "$PNAME" == "DEFAULT" ];then
		echo -e "\033[33m\033[1mbuild_clean $1 $2 for HOST: $HOST PRODUCT: not set\033[0m"
	else
    	echo -e "\033[33m\033[1mbuild_clean $1 $2 for HOST: $HOST PRODUCT: PRODUCT_$PNAME\033[0m"
	fi
	make clean -f build/core/main.mk -j$CPU_NUM SOURCES_DIR=$1 TARGET_NAME=$2 PROJECT_ROOT=$PROJECT_ROOT HOST=$HOST HOST_FNAME=$HOST_FNAME PRODUCT_NAME=$PNAME
}

function mbuild_remake()
{
    if [ $# -lt 2 -o $# -gt 3 ];then
        echo "[usage] mbuild_remake <directory> <target> <libtype>"
		return
    fi
	PNAME="$(echo $PRODUCT_NAME | tr '[:lower:]' '[:upper:]')"
    mbuild_clean $1 $2
    mbuild_make $1 $2 $3
}