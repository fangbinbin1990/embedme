/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __THREAD_SYNC_H__
#define __THREAD_SYNC_H__

#ifdef OS_CYGWIN

#endif

#include "BaseType.h"

#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <semaphore.h>
#include <pthread.h>
#include <queue>

namespace libemb{
/**
 *  \file   ThreadUtil.h   
 *  \class  Mutex
 *  \brief  互斥锁类	
 */
class Mutex{
public:
    Mutex();
    ~Mutex();
    int lock();
    int unLock();
    int tryLock();
private:
    pthread_mutex_t m_mutex;
	friend class MutexCond;
};
/**
 *  \file   ThreadUtil.h   
 *  \class  AutoLock
 *  \brief  自动释放的互斥锁类	
 */
class AutoLock{
public:
    AutoLock(Mutex& mutex);
    virtual ~AutoLock();
private:
    Mutex* m_pMutex;
};

/**
 *  \file   ThreadUtil.h   
 *  \class  MutexCond
 *  \brief  条件变量
 */
class MutexCond : public Mutex{
public:
    MutexCond();
    ~MutexCond();
    int wait(int usec=-1);
    int meet();
private:
    pthread_cond_t m_cond;
    pthread_condattr_t m_condAttr;
};

/**
 *  \file   ThreadUtil.h   
 *  \class  Semaphore
 *  \brief  信号量类(基于POSIX接口)	
 */
class Semaphore{
public:
    Semaphore(char* name=NULL);/* name=NULL时表示创建无名信号量 */
    ~Semaphore();
    bool open(int value=1);
    bool close();
	bool unlink();
    bool wait();
    bool tryWait();
    bool post();
    bool getValue(int& value);
protected:
    sem_t m_sem;
	std::string m_name;
};

/**
 *  \file   ThreadUtil.h   
 *  \class  AtomicVar
 *  \brief  原子变量.
 */
template <class Type>
class AtomicVar{
public:
    AtomicVar();
    ~AtomicVar();
    Type getValue(void);
    void setValue(Type value);
	void waitAndSet(Type wait,Type value);
    bool testValue(Type value);
private:
    Mutex m_mutex;
    Type m_value;
};

template <class Type>
AtomicVar<Type>::AtomicVar()
{}

template <class Type>
AtomicVar<Type>::~AtomicVar()
{}


template <class Type>
Type AtomicVar<Type>::getValue()
{
    AutoLock lock(m_mutex);
    return m_value;
}

template <class Type>
void AtomicVar<Type>::setValue(Type value)
{
    AutoLock lock(m_mutex);
    m_value = value;
}

template <class Type>
void AtomicVar<Type>::waitAndSet(Type wait,Type value)
{
	 AutoLock lock(&m_mutex);
	 while(m_value!=wait)
	 {
		for(auto i=0; i<10000; i++);
	 }
	 m_value = value;
}

template <class Type>
bool AtomicVar<Type>::testValue(Type value)
{
    AutoLock lock(m_mutex);
    return (m_value==value)?true:false;
}

/**
 *  \file   ThreadUtil.h   
 *  \class  AtomicVector
 *  \brief  原子向量.
 */
template <class Type>
class AtomicVector{
public:
    AtomicVector();
    ~AtomicVector();
    void push_back(Type element);   /* 在最后插入一个元素 */
    void pop_back(void);            /* 删除最后一个元素 */
    Type front(void);
    Type back(void);
    void clear(void);               /* 清空所有元素 */
    bool empty(void);               /* 如果vector为空则返回真 */
    uint32 size(void);              /* 返回vector中元素的个数 */
    Type& operator[ ](int idx);     /* 数组操作符 */

private:
    Mutex m_mutex;
    std::vector<Type> m_vector;
};

template <class Type>
AtomicVector<Type>::AtomicVector()
{}

template <class Type>
AtomicVector<Type>::~AtomicVector()
{}

template <class Type>
void AtomicVector<Type>::push_back(Type element)
{
    m_mutex.lock();
    m_vector.push_back(element);
    m_mutex.unLock();
}

template <class Type>
void AtomicVector<Type>::pop_back()
{
    m_mutex.lock();
    m_vector.pop_back();
    m_mutex.unLock();
}

template <class Type>
Type AtomicVector<Type>::front()
{
    m_mutex.lock();
    Type ret=m_vector.front();
    m_mutex.unLock();
    return ret;
}

template <class Type>
Type AtomicVector<Type>::back()
{
    m_mutex.lock();
    Type ret=m_vector.back();
    m_mutex.unLock();
    return ret;
}

template <class Type>
void AtomicVector<Type>::clear(void)
{
    m_mutex.lock();
    m_vector.clear();
    m_mutex.unLock();
}

template <class Type>
bool AtomicVector<Type>::empty(void)
{
    m_mutex.lock();
    bool ret = m_vector.empty();
    m_mutex.unLock();
    return ret;
}

template <class Type>
unsigned int AtomicVector<Type>::size(void)
{
    m_mutex.lock();
    uint32 ret = m_vector.size();
    m_mutex.unLock();
    return ret;
}

template <class Type>
Type& AtomicVector<Type>::operator[](int idx)
{
    m_mutex.lock();
    Type& ret = m_vector[idx];
    m_mutex.unLock();
    return ret;
}

/**
 *  \file   ThreadUtil.h   
 *  \class  AtomicQueue
 *  \brief  原子队列.
 */
template <class Type>
class AtomicQueue{
public:
    AtomicQueue();
    ~AtomicQueue();
    void push(Type element);    /* 在末尾加入一个元素 */
    void pop(void);             /* 删除第一个元素 */
    Type front(void);           /* 返回第一个元素 */
    Type back(void);            /* 返回最后一个元素 */
	void clear(void);           /* 清空所有元素 */
    bool empty(void);           /* 如果队列空则返回真 */
    uint32 size(void);          /* 返回队列中元素的个数 */
private:
    Mutex m_mutex;
    std::queue<Type> m_queue;
};

template <class Type>
AtomicQueue<Type>::AtomicQueue()
{}

template <class Type>
AtomicQueue<Type>::~AtomicQueue()
{}

template <class Type>
void AtomicQueue<Type>::push(Type element)
{
    m_mutex.lock();
    m_queue.push(element);
    m_mutex.unLock();
}

template <class Type>
void AtomicQueue<Type>::pop(void)
{
    m_mutex.lock();
    m_queue.pop();
    m_mutex.unLock();
}

template <class Type>
Type AtomicQueue<Type>::front(void)
{
    m_mutex.lock();
    Type ret = m_queue.front();
    m_mutex.unLock();
    return ret;
}

template <class Type>
Type AtomicQueue<Type>::back(void)
{
    m_mutex.lock();
    Type ret = m_queue.back();
    m_mutex.unLock();
    return ret;
}

template <class Type>
void AtomicQueue<Type>::clear(void)
{
    m_mutex.lock();
    std::queue<Type>().swap(m_queue);
    m_mutex.unLock();
}

template <class Type>
bool AtomicQueue<Type>::empty(void)
{
    m_mutex.lock();
    bool ret = m_queue.empty();
    m_mutex.unLock();
    return ret;
}

template <class Type>
unsigned int AtomicQueue<Type>::size(void)
{
    m_mutex.lock();
    unsigned int ret = m_queue.size();
    m_mutex.unLock();
    return ret;
}
}

#endif
