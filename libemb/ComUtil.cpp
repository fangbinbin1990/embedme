/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#include "ComUtil.h"
#include "Tracer.h"
#include "FileUtil.h"

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <ctype.h>
#include <libgen.h>
#include <math.h>

namespace libemb{
using namespace std;

bool ComUtil::equal(float f1, float f2, float precision)
{
	return (fabs(f1-f2)<precision)?true:false;
}

/**
 *  \brief  生成一个随机数
 *  \param  max 最大取值
 *  \return 返回随机数
 *  \note   生成一个0~max之间的随机数
 */
int ComUtil::random(int max)
{
    if (max<=0)
    {
        max = RAND_MAX;
    }
    struct timeval current;
    unsigned int seed;
    gettimeofday(&current,NULL);
    seed = current.tv_sec+current.tv_usec;
    srand((unsigned)seed);
    return rand()%max;
}

/**
 *  \brief  判断主机是大端还是小端字节序
 *  \param  void
 *  \return 大端字节序返回true,小端字节序返回false
 *  \note   小端模式是数字的低位在低地址,大端模式是高位在低地址
 *			0x1234				---数值
 *			[0x34][0x12]---小端模式中内存存储(计算机逻辑,低地址放低位)
 *          [0x12][0x34]---大端模式中内存存储(更符合人类思维习惯)
 */
bool ComUtil::isBigEndian(void)
{
	union EndianWrap_U
	{
		sint32 a;
		uint8  b;
	};
	union EndianWrap_U ew;
	ew.a=0x01;
	return (ew.b)?false:true;
}

/**
 *  \brief  将数字转换成小端字节序
 *  \param  value 16位宽的数字
 *  \return 返回转换后的数字
 *  \note   如果本身机器字节序就是小端，该函数将不会改变value的字节序.
 */
uint16 ComUtil::host2LitEndian(uint16 value)
{
	union EndianWrap_U
	{
		uint16 v;
		char  c[2];
	};
	if (isBigEndian())
	{
		union EndianWrap_U ew1,ew2;
		ew1.v = value;
		ew2.c[0] = ew1.c[1];
		ew2.c[1] = ew1.c[0];
		return ew2.v;
	}
	return value;
}

/**
 *  \brief  将数字转换成小端字节序
 *  \param  value 32位宽的数字
 *  \return 返回转换后的数字
 *  \note   如果本身机器字节序就是小端，该函数将不会改变value的字节序.
 */
uint32 ComUtil::host2LitEndian(uint32 value)
{
	union EndianWrap_U
	{
		uint32 v;
		char  c[4];
	};
	if (isBigEndian())
	{
		union EndianWrap_U ew1,ew2;
		ew1.v= value;
		ew2.c[0] = ew1.c[3];
		ew2.c[1] = ew1.c[2];
		ew2.c[2] = ew1.c[1];
		ew2.c[3] = ew1.c[0];
		return ew2.v;
	}
	return value;
}

/**
 *  \brief  将数字转换成小端字节序
 *  \param  value float型值
 *  \return 返回转换后的数字
 *  \note   none
 */
float ComUtil::host2LitEndian(float value)
{
	union EndianWrap_U
	{
		float v;
		char  c[4];
	};
	if (isBigEndian())
	{
		union EndianWrap_U ew1,ew2;
		ew1.v = value;
		ew2.c[0] = ew1.c[3];
		ew2.c[1] = ew1.c[2];
		ew2.c[2] = ew1.c[1];
		ew2.c[3] = ew1.c[0];
		return ew2.v;
	}
	return value;
}



/**
 *  \brief  将数字转换成大端字节序
 *  \param  value 16位宽的数字
 *  \return 返回转换后的数字
 *  \note   如果本身机器字节序就是大端，该函数将不会改变value的字节序.
 */
uint16 ComUtil::host2BigEndian(uint16 value)
{
	union EndianWrap_U
	{
		uint16 v;
		char  c[2];
	};
	if (!isBigEndian())
	{
		union EndianWrap_U ew1,ew2;
		ew1.v = value;
		ew2.c[0] = ew1.c[1];
		ew2.c[1] = ew1.c[0];
		return ew2.v;
	}
	return value;
}

/**
 *  \brief  将数字转换成大端字节序
 *  \param  value 32位宽的数字
 *  \return 返回转换后的数字
 *  \note   如果本身机器字节序就是大端，该函数将不会改变value的字节序.
 */
uint32 ComUtil::host2BigEndian(uint32 value)
{
	union EndianWrap_U
	{
		uint32 v;
		char  c[4];
	};
	if (!isBigEndian())
	{
		union EndianWrap_U ew1,ew2;
		ew1.v = value;
		ew2.c[0] = ew1.c[3];
		ew2.c[1] = ew1.c[2];
		ew2.c[2] = ew1.c[1];
		ew2.c[3] = ew1.c[0];
		return ew2.v;
	}
	return value;
}

/**
 *  \brief  将数字转换成大端字节序
 *  \param  value float型值
 *  \return 返回转换后的数字
 *  \note	none
 */
float ComUtil::host2BigEndian(float value)
{
	union EndianWrap_U
	{
		float v;
		char  c[4];
	};
	if (!isBigEndian())
	{
		union EndianWrap_U ew1,ew2;
		ew1.v = value;
		ew2.c[0] = ew1.c[3];
		ew2.c[1] = ew1.c[2];
		ew2.c[2] = ew1.c[1];
		ew2.c[3] = ew1.c[0];
		return ew2.v;
	}
	return value;
}


/**
 *  \brief  unicode编码转换为utf8编码的字符串
 *  \param  unicode 字符编码
 *  \return 返回转换后的字符,转换失败时返回空字符
 *  \note   none
 */
std::string ComUtil::unicodeOneToUtf8String(uint32 unicode)
{
    int utflen=0;
    char utf8code[7]={0};

    unicode=host2LitEndian(unicode);

    if ( unicode <= 0x0000007F )
    {
        // * U-00000000 - U-0000007F:  0xxxxxxx
        utf8code[0]     = (unicode & 0x7F);
        utflen = 1;
    }
    else if ( unicode >= 0x00000080 && unicode <= 0x000007FF )
    {
        // * U-00000080 - U-000007FF:  110xxxxx 10xxxxxx
        utf8code[1] = (unicode & 0x3F) | 0x80;
        utf8code[0] = ((unicode >> 6) & 0x1F) | 0xC0;
        utflen = 2;
    }
    else if ( unicode >= 0x00000800 && unicode <= 0x0000FFFF )
    {
        // * U-00000800 - U-0000FFFF:  1110xxxx 10xxxxxx 10xxxxxx
        utf8code[2] = (unicode & 0x3F) | 0x80;
        utf8code[1] = ((unicode >>  6) & 0x3F) | 0x80;
        utf8code[0] = ((unicode >> 12) & 0x0F) | 0xE0;
        utflen = 3;
    }
    else if ( unicode >= 0x00010000 && unicode <= 0x001FFFFF )
    {
        // * U-00010000 - U-001FFFFF:  11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
        utf8code[3] = (unicode & 0x3F) | 0x80;
        utf8code[2] = ((unicode >>  6) & 0x3F) | 0x80;
        utf8code[1] = ((unicode >> 12) & 0x3F) | 0x80;
        utf8code[0] = ((unicode >> 18) & 0x07) | 0xF0;
        utflen = 4;
    }
    else if ( unicode >= 0x00200000 && unicode <= 0x03FFFFFF )
    {
        // * U-00200000 - U-03FFFFFF:  111110xx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx
        utf8code[4] = (unicode & 0x3F) | 0x80;
        utf8code[3] = ((unicode >>  6) & 0x3F) | 0x80;
        utf8code[2] = ((unicode >> 12) & 0x3F) | 0x80;
        utf8code[1] = ((unicode >> 18) & 0x3F) | 0x80;
        utf8code[0] = ((unicode >> 24) & 0x03) | 0xF8;
        utflen = 5;
    }
    else if ( unicode >= 0x04000000 && unicode <= 0x7FFFFFFF )
    {
        // * U-04000000 - U-7FFFFFFF:  1111110x 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx
        utf8code[5] = (unicode & 0x3F) | 0x80;
        utf8code[4] = ((unicode >>  6) & 0x3F) | 0x80;
        utf8code[3] = ((unicode >> 12) & 0x3F) | 0x80;
        utf8code[2] = ((unicode >> 18) & 0x3F) | 0x80;
        utf8code[1] = ((unicode >> 24) & 0x3F) | 0x80;
        utf8code[0] = ((unicode >> 30) & 0x01) | 0xFC;
        utflen = 6;
    }
    std::string utf8str = utf8code;
    return utf8str;

}
/**
 *  \brief  utf8编码字串转换为unicode编码
 *  \param  utfcode UTF8编码(1~6字节)
 *  \return 成功返回unicode编码,失败返回0
 *  \note   none
 */
uint32 ComUtil::utf8OneToUnicode(const char* utf8code)
{
    // b1 表示UTF-8编码的高字节, b2 表示次高字节, ...
    uint8 b1, b2, b3, b4, b5, b6;

    uint8 utfbytes=0;
    uint8 tmp=utf8code[0];
    while((tmp&0x80)!=0)
    {
        utfbytes++;
        tmp = tmp<<1;
    }

    uint32 unicode = 0x0;
    uint8 *unibuf = (uint8*)&unicode;
    switch (utfbytes)
    {
        case 0:
            unibuf[0] = utf8code[0];
            break;
        case 2:
            b1 = utf8code[0];
            b2 = utf8code[1];
            if ((b2&0xE0) != 0x80)
            {
                return 0;
            }
            unibuf[0] = (b1<<6) + (b2&0x3F);
            unibuf[1] = (b1>>2)&0x07;
            break;
        case 3:
            b1 = utf8code[0];
            b2 = utf8code[1];
            b3 = utf8code[2];
            if (((b2&0xC0) != 0x80) ||
                ((b3&0xC0) != 0x80))
            {
                return 0;
            }
            unibuf[0] = (b2<<6) + (b3&0x3F);
            unibuf[1] = (b1<<4) + ((b2>>2)&0x0F);
            break;
        case 4:
            b1 = utf8code[0];
            b2 = utf8code[1];
            b3 = utf8code[2];
            b4 = utf8code[3];
            if (((b2&0xC0) != 0x80) ||
                ((b3&0xC0) != 0x80)  ||
                ((b4&0xC0) != 0x80))
            {
                return 0;
            }
            unibuf[0] = (b3<<6) + (b4&0x3F);
            unibuf[1] = (b2<<4) + ((b3>>2)&0x0F);
            unibuf[2] = ((b1<<2)&0x1C)  + ((b2>>4)&0x03);
            break;
        case 5:
            b1 = utf8code[0];
            b2 = utf8code[1];
            b3 = utf8code[2];
            b4 = utf8code[3];
            b5 = utf8code[4];
            if (((b2 & 0xC0) != 0x80) ||
                ((b3 & 0xC0) != 0x80)  ||
                ((b4 & 0xC0) != 0x80) || ((b5 & 0xC0) != 0x80) )
            {
                return 0;
            }
            unibuf[0] = (b4<<6) + (b5&0x3F);
            unibuf[1] = (b3<<4) + ((b4>>2)&0x0F);
            unibuf[2] = (b2<<2) + ((b3>>4)&0x03);
            unibuf[3] = (b1<<6);
            break;
        case 6:
            b1 = utf8code[0];
            b2 = utf8code[1];
            b3 = utf8code[2];
            b4 = utf8code[3];
            b5 = utf8code[4];
            b6 = utf8code[5];
            if (((b2&0xC0) != 0x80) ||
                ((b3&0xC0) != 0x80) ||
                ((b4&0xC0) != 0x80) ||
                ((b5&0xC0) != 0x80) ||
                ((b6&0xC0) != 0x80) )
            {
                return 0;
            }
            unibuf[0] = (b5<< 6) + (b6 & 0x3F);
            unibuf[1] = (b5<< 4) + ((b6 >> 2)&0x0F);
            unibuf[2] = (b3<< 2) + ((b4 >> 4)&0x03);
            unibuf[3] = ((b1<<6)&0x40) + (b2&0x3F);
            break;
        default:
            return 0;
            break;
    }
    return unicode;
}

/**
 *  \brief  位反转
 *  \param
 *  \return 返回结果
 *  \note
 */
uint16 ComUtil::bitsReverse(uint16 ref, uint8 bits)
{
	uint16 value = 0;
	for (uint16 i=1; i<(bits+1); i++)
	{
		if (ref&0x01)
        {
            value |= 1 << (bits - i);
        }
		ref >>= 1;
	}
	return value;
}

/**
 *  \brief  算术计算函数
 *  \param  expression 算术表达式,支持整数的加减乘除及括号
 *  \return 返回计算结果
 *  \note
 */
int ComUtil::eval(const char* expression)
{
    return 0;
}
}

