/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014-2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __COMMANDPIPE_H__
#define __COMMANDPIPE_H__

/**
 *  \file   ProcUtil.h
 *  \class  ProcUtil
 *  \brief  进程工具类.
 */

#include "BaseType.h"
#include "IODevice.h"
#include "StrUtil.h"

namespace libemb{
class ProcUtil{
public:
    ProcUtil(){};
    ~ProcUtil(){};
    static int execute(std::string cmd, std::string& resultStr, int timeoutSec=-1);/* -1:等待进程退出,其他值:最长等待时间 */
    static std::string execute(std::string cmd, int timeoutSec=-1);
	static int createProc(std::string procName, std::string args,std::string envs,int delays=0);
	static void killProc(std::string procName);
	static std::vector<int> getPidsByName(const char* processName);
};

/* FIFO：一种先入先出的有名管道,可以在任意两个进程间进行通信 */
class ProcFifo:public IODevice{
public:
    ProcFifo();
    virtual ~ProcFifo();
    virtual bool open(const char *devName, int ioMode = IO_MODE_RDWR_ONLY); 
};

}
#endif
