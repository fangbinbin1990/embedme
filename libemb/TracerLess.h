#ifndef __TRACER_LESS_H__
#define __TRACER_LESS_H__

/* 此文件用于消除打印,在实时系统中,为了满足系统实时性,我们有时有必要关闭串口打印.
 * 这种情况下，我们只需要在源码中包含本文件(在include "Tracer.h"之后),即可立即消
 * 除打印.示例代码:
 * #include "Tracer.h"
 * #include "TracerLess.h" //调试时我们可以屏蔽这句,那样会正常打印
 * ...
 * TRACE_ERR("..."); //当我们包含TracerNone.h时,此时将不打印
 * ...
 */

#ifdef TRACE_DBG
#undef TRACE_DBG 
#define TRACE_DBG(fmt,arg...)   
#endif

#ifdef TRACE_INFO
#undef TRACE_INFO 
#define TRACE_INFO(fmt,arg...)
#endif

#ifdef TRACE_WARN
#undef TRACE_WARN 
#define TRACE_WARN(fmt,arg...)
#endif

#ifdef TRACE_DBG_CLASS
#undef TRACE_DBG_CLASS 
#define TRACE_DBG_CLASS(fmt,arg...)
#endif

#ifdef TRACE_INFO_CLASS
#undef TRACE_INFO_CLASS 
#define TRACE_INFO_CLASS(fmt,arg...)
#endif

#ifdef TRACE_WARN_CLASS
#undef TRACE_WARN_CLASS 
#define TRACE_WARN_CLASS(fmt,arg...)
#endif

#ifdef TRACE_HEX
#undef TRACE_HEX 
#define TRACE_HEX(tag,buf,len)
#endif

#ifdef TRACE_TEXT
#undef TRACE_TEXT 
#define TRACE_TEXT(fmt,arg...)
#endif


#endif
