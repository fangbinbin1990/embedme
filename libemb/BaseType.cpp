/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#include "BaseType.h"
#include "Tracer.h"
#include "StrUtil.h"
#include <stdio.h>
#include <stdlib.h>

using namespace std;
namespace libemb{

/**
 *  \brief  从格式化的字符串中初始化数组
 *  \param  arrayString 格式化字符串,例如:[0,1,2,3]
 *  \return 成功返回true,失败返回false
 */
bool IntArray::initWithString(const std::string & arrayString)
{
    m_array.clear();
    std::vector<std::string> strArray = StrUtil::cutString(arrayString, "[", "]", ",");
    int arraySize = strArray.size();
    if (arraySize>0)
    {
        for(auto i=0; i<arraySize; i++)
        {
            m_array.push_back(StrUtil::stringToInt(strArray[i]));
        }
        return true;
    }
    return false;
}

/**
 *  \brief  序列化字符串数组
 *  \param  none
 *  \return 返回格式化字符串:[0,1,2]
 */
std::string IntArray::serialize()
{
    int arraySize = m_array.size();
    std::string arrayString="[";
    for(auto i=0; i<arraySize; i++)
    {
        char buf[32]={0};
        sprintf(buf,"%d",m_array[i]);
        arrayString += buf;
        if (i<(arraySize-1))
        {
            arrayString += ",";
        }
    }
    arrayString += "]";
    return arrayString;
}


/**
 *  \brief  从格式化的字符串中初始化数组
 *  \param  arrayString 格式化字符串,例如:[0.123,1.234,2.345]
 *  \return 成功返回true,失败返回false
 */
bool DoubleArray::initWithString(const std::string & arrayString)
{
    m_array.clear();
    std::vector<std::string> strArray = StrUtil::cutString(arrayString, "[", "]", ",");
    int arraySize = strArray.size();
    if (arraySize>0)
    {
        for(auto i=0; i<arraySize; i++)
        {
            m_array.push_back(atof(CSTR(strArray[i])));
        }
        return true;
    }
    return false;
}

/**
 *  \brief  序列化字符串数组
 *  \param  none
 *  \return 返回格式化字符串:[0.123,1.234,2.345]
 */
std::string DoubleArray::serialize()
{
    int arraySize = m_array.size();
    std::string arrayString="[";
    for(auto i=0; i<arraySize; i++)
    {
        char buf[32]={0};
        sprintf(buf,"%lf",m_array[i]);
        arrayString += buf;
        if (i<(arraySize-1))
        {
            arrayString += ",";
        }
    }
    arrayString += "]";
    return arrayString;
}

/**
 *  \brief  从格式化的字符串中初始化数组
 *  \param  arrayString 格式化字符串,例如:["Jim","Tom","Kim"]
 *  \return 成功返回true,失败返回false
 */
bool StringArray::initWithString(const std::string & arrayString)
{
    m_array.clear();
    std::string tmpString = StrUtil::trimTailBlank(arrayString);
    int strLen = tmpString.size();
    
    if (strLen<2 ||
        tmpString[0]!='[' ||
        tmpString[strLen-1]!=']')
    {
        return false;
    }
    int quotNum=0;
    int commaNum=0;
    std::string item;
    for(auto i=1; i<strLen-1; i++)
    {
        if (tmpString[i]=='\"')
        {
            quotNum++;
            if (quotNum%2==1)
            {
                item = "";
                commaNum = 0;
            }
            else
            {
                m_array.emplace_back(item);
            }
            continue;
        }
        else
        {
            if (quotNum%2==0)
            {
                if (tmpString[i]==' ')
                {
                    continue;
                }
                else if (tmpString[i]==',')
                {
                    commaNum++;
                    if (commaNum>1)
                    {
                        item="";
                        m_array.emplace_back(item);
                    }
                }
                else
                {
                    m_array.clear();
                    return false;
                }
            }
            else
            {
                item.append(1,tmpString[i]);                  
            }
        }
    }
    if (quotNum%2==1)
    {
        m_array.clear();
        return false; 
    }
    return true;
}

/**
 *  \brief  序列化字符串数组
 *  \param  none
 *  \return 返回格式化字符串:["a","b","c"]
 */
std::string StringArray::serialize()
{
    int arraySize = m_array.size();
    std::string arrayString="[";
    for(auto i=0; i<arraySize; i++)
    {
        arrayString += "\"";
        arrayString += m_array[i];
        arrayString += "\"";
        if (i<(arraySize-1))
        {
            arrayString += ",";
        }
    }
    arrayString += "]";
    return arrayString;
}

/**
 *  \brief  元组元素构造函数
 *  \param  value 整型
 *  \return none
 */
TupleItem::TupleItem(int value)
{
    m_type = BASETYPE_INT;
    m_value = value;
    m_string = StrUtil::stringFormat("%d",(int)m_value); 
}
/**
 *  \brief  元组元素构造函数
 *  \param  value 浮点型
 *  \return none
 */
TupleItem::TupleItem(double value)
{
    m_type = BASETYPE_DOUBLE;
    m_value = value;
    m_string = StrUtil::stringFormat("%f",m_value); 
}
/**
 *  \brief  元组元素构造函数
 *  \param  value 字符串型
 *  \return none
 */
TupleItem::TupleItem(std::string value)
{
    m_type = BASETYPE_STRING;
    m_string = value;
    m_value = atof(CSTR(m_string));
}
/**
 *  \brief  元组元素拷贝构造函数
 *  \param  item 元组元素
 *  \return none
 */
TupleItem::TupleItem(const TupleItem& item)
{
    m_type = item.m_type;
    m_value = item.m_value;
    m_string = item.m_string;
}
/**
 *  \brief  元组元素析构函数
 *  \param  none
 *  \return none
 */
TupleItem::~TupleItem()
{
}
/**
 *  \brief  获取元组元素类型
 *  \param  none
 *  \return 共三种类型:BASETYPE_INT,BASETYPE_DOUBLE,BASETYPE_STRING.
 */
int TupleItem::baseType()
{
    return m_type;
}

/**
 *  \brief  元组元素转成int型
 *  \param  none
 *  \return 元组元素的实际类型值
 */
int TupleItem::toInt()
{
    return (int)m_value;
}
/**
 *  \brief  元组元素转成double型
 *  \param  none
 *  \return 元组元素的实际类型值
 */
double TupleItem::toDouble()
{
    return m_value;
}
/**
 *  \brief  元组元素转成string型
 *  \param  none
 *  \return 元组元素的实际类型值
 */
std::string TupleItem::toString()
{
    return m_string;
}

/**
 *  \brief  元组构造函数
 *  \param  none
 *  \return none
 */
Tuple::Tuple()
{
}
/**
 *  \brief  元组拷贝构造函数
 *  \param  tuple 元组
 *  \return none
 */
Tuple::Tuple(const Tuple&tuple)
{
    int num=tuple.m_itemVect.size();
    for(auto i=0; i<num; i++)
    {
        switch (tuple.m_itemVect[i]->baseType())
        {
            case BASETYPE_INT:
            {
            	auto itemPtr = std::make_unique<TupleItem>(tuple.m_itemVect[i]->toInt());
                m_itemVect.push_back(std::move(itemPtr));
                break;
            }
            case BASETYPE_DOUBLE:
            {
            	auto itemPtr = std::make_unique<TupleItem>(tuple.m_itemVect[i]->toDouble());
                m_itemVect.push_back(std::move(itemPtr));
                break;
            }
            case BASETYPE_STRING:
            {
            	auto itemPtr = std::make_unique<TupleItem>(tuple.m_itemVect[i]->toString());
                m_itemVect.push_back(std::move(itemPtr));
                break;
            }
            default:
                break;
        }
    }
}
/**
 *  \brief  元组析构函数
 *  \param  none
 *  \return none
 */
Tuple::~Tuple()
{
    clear();
}
/**
 *  \brief  从格式化的字符串中初始化元组
 *  \param  tupleString 格式化字符串,例如:(1,"Tom",3.1415926)
 *  \return 成功返回true,失败返回false
 */
bool Tuple::initWithString(const std::string& tupleString)
{
    m_itemVect.clear();
    string tmpString = StrUtil::trimTailBlank(tupleString);
    int strLen = tmpString.size();
    if (strLen<2 ||
        tmpString[0]!='(' ||
        tmpString[strLen-1]!=')')
    {
        return false;
    }
    int quotNum=0;
    int commaNum=0;
    int type = BASETYPE_NONE;
    std::string item;
    for(auto i=1; i<strLen-1; i++)
    {
        if (tmpString[i]!=' ' &&
            tmpString[i]!=',' &&
            tmpString[i]!='\"')
        {
            if (commaNum!=0)
            {
                TRACE_ERR_CLASS("Invalid tuple: %s,i=%d\n",CSTR(tmpString),i);
                return false;
            }
            item.append(1,tmpString[i]);
            continue;
        }
        if (tmpString[i]=='\"')
        {
            quotNum++;
            if (quotNum%2==0)/* 字符串结束 */
            {
                this->append(item);
                commaNum = 0;
                type = BASETYPE_NONE;
            }
            else /* 字符串开始 */
            {
                type = BASETYPE_STRING;
            }
            item.clear();
            continue;
        }
        else if (tmpString[i]==' ')
        {
            if (type==BASETYPE_STRING)
            {
                item.append(1,tmpString[i]);
            }
            continue;
        }
        else if (tmpString[i]==',')
        {
            if (type==BASETYPE_STRING)/* 字符串里面的逗号 */
            {
                item.append(1,tmpString[i]);
            }
            else /* 当前元素结束 */
            {
                commaNum++;
                if (commaNum<=1)
                {
                    if (!item.empty())
                    {
                        if (item.find(".")!=string::npos)
                        {
                        	double val = atof(CSTR(item));
                            this->append(val);    
                        }
                        else
                        {
                        	int val = atoi(CSTR(item));
                            this->append(val);
                        }
                    }
                    item.clear();
                    commaNum=0;
                    type=BASETYPE_NONE;
                    continue;
                }
                else
                {
                    clear();
                    return false;
                }   
            }
            continue;
        }
    }
    if (!item.empty())
    {
        if (item.find(".")!=std::string::npos)
        {
        	double val = atof(CSTR(item));
            this->append(val);    
        }
        else
        {
        	int val = atoi(CSTR(item));
            this->append(val);
        }
    }
    return true;
}

/**
 *  \brief  获取元组大小
 *  \param  none
 *  \return 元组所包含的元素的个数
 */
int Tuple::size()
{
    return m_itemVect.size();
}

/**
 *  \brief  获取类型
 *  \param  none
 *  \return BASETYPE_TUPLE
 */
int Tuple::type()
{
    return BASETYPE_TUPLE;
}

/**
 *  \brief  清空元组
 *  \param  none
 *  \return none
 */
void Tuple::clear()
{
	m_itemVect.clear();
}

/**
 *  \brief  序列化元组
 *  \param  none
 *  \return 序列化字符串
 */
std::string Tuple::serialize()
{
    std::string result="(";
    int num = m_itemVect.size();
    for(auto i=0; i<num; i++)
    {
        switch (m_itemVect[i]->baseType())
        {
            case BASETYPE_INT:
            {
                char buf[32]={0};
                sprintf(buf,"%d",m_itemVect[i]->toInt());
                result += std::string(buf);
                break;
            }
            case BASETYPE_DOUBLE:
            {
                char buf[32]={0};
                sprintf(buf,"%lf",m_itemVect[i]->toDouble());
                result += std::string(buf);
                break;
            }
            case BASETYPE_STRING:
            {
                result += "\"";
                result += m_itemVect[i]->toString();
                result +="\"";
                break;
            }
            default:
                return "";
        }
        if (i!=(num-1))
        {
            result += ",";
        }
    }    
    result += ")";
    return result;
}
/**
 *  \brief  []运算符重载
 *  \param  none
 *  \return 返回元组元素
 */
TupleItem& Tuple::operator[](int idx)
{
    TupleItem& ret = *(m_itemVect[idx]);
    return ret;
}

/**
 *  \brief  赋值运算符重载
 *  \param  none
 *  \return 返回元组
 */
Tuple& Tuple::operator=(const Tuple& tuple)
{
    if (this==&tuple)
    {
        return (*this);
    }
    /* 清除所有元素 */
    clear();
    /* 重新拷贝tuple的元素 */
    int num = tuple.m_itemVect.size();
    for(auto i=0; i<num; i++)
    {
        switch (tuple.m_itemVect[i]->baseType())
        {
            case BASETYPE_INT:
            {
            	auto itemPtr = std::make_unique<TupleItem>(tuple.m_itemVect[i]->toInt());
                m_itemVect.push_back(std::move(itemPtr));
                break;
            }
            case BASETYPE_DOUBLE:
            {
            	auto itemPtr = std::make_unique<TupleItem>(tuple.m_itemVect[i]->toDouble());
                 m_itemVect.push_back(std::move(itemPtr));
                break;
            }
            case BASETYPE_STRING:
            {
            	auto itemPtr = std::make_unique<TupleItem>(tuple.m_itemVect[i]->toString());
                m_itemVect.push_back(std::move(itemPtr));
                break;
            }
            default:
                break;
        }
    }
    return (*this);
}

}