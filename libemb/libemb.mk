LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := libemb

LOCAL_CFLAGS :=
LOCAL_CXXFLAGS :=
LOCAL_LDFLAGS :=
LOCAL_INC_PATHS := \
	$(LOCAL_PATH)

LOCAL_SRC_FILES := \
	ArgUtil.cpp \
	BaseType.cpp \
	ComUtil.cpp \
	Coroutine.cpp \
	DateTime.cpp \
	FileUtil.cpp \
	FSMachine.cpp \
	IODevice.cpp \
	Logger.cpp \
	Memory.cpp \
	MsgQueue.cpp \
	NetUtil.cpp \
	Pollset.cpp \
	Pluglet.cpp \
	ProcUtil.cpp \
	RegExp.cpp \
	RingBuffer.cpp \
	SerialPort.cpp \
	Socket.cpp \
	StrUtil.cpp \
	Thread.cpp \
	ThreadUtil.cpp \
	Timer.cpp \
	Tracer.cpp


LOCAL_LIB_TYPE:=static

include $(BUILD_LIBRARY)