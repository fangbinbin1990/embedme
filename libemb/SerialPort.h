/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __SERIALPORT_H__
#define __SERIALPORT_H__

#include "IODevice.h"

#include <termios.h>
#include <unistd.h>
namespace libemb{
/** 串口波特率值 */
enum BAUDRATE_E
{
    BAUDRATE_1200      = 1200,
    BAUDRATE_2400      = 2400,
    BAUDRATE_4800      = 4800,
    BAUDRATE_9600      = 9600,
    BAUDRATE_19200     = 19200,
    BAUDRATE_38400     = 38400,
    BAUDRATE_57600     = 57600,
    BAUDRATE_115200    = 115200,
    BAUDRATE_460800    = 460800
};

/** 串口数据位值 */
enum DATABITS_E
{
    DATABITS_5=5,
    DATABITS_6,
    DATABITS_7,
    DATABITS_8
};

/** 串口停止位值 */
enum STOPBITS_E
{
    STOPBITS_1=1,
    STOPBITS_2
};

/** 串口校验位值 */
enum PARITY_E
{
    PARITY_NONE=0,
    PARITY_ODD,
    PARITY_EVEN,
    PARITY_SPACE
};

/** 串口流控位值 */
enum FLOWCTRL_E{
    FLOWCTRL_NONE=0,
    FLOWCTRL_HW,
    FLOWCTRL_SW
};

enum DATAMODE_E{
    DATAMODE_RS232=0,	/* 数字全双工 */
    DATAMODE_RS422,		/* 差分全双工 */
    DATAMODE_RS485,		/* 差分半双工 */
};

/** RS485方向属性(由于RS485是半双工的,读写前需要设置方向) */
enum RS485DIR_E{
    RS485DIR_RX=0,
    RS485DIR_TX,
};

class SerialPortAttr{
public:
	int m_bandrate;
	int m_databits;
	int m_stopbits;
	int m_parity;
	int m_flowctrl;
	int m_datamode;
	int m_rs485dir;
};

/**
 *  \file   SerialPort.h   
 *  \class  SerialPort
 *  \brief  串口设备类.
 */
class SerialPort:public IODevice{
public:
	/** 串口属性类型 */
	enum SERIAL_ATTR_E
	{
	    ATTR_BAUDRATE=0,
	    ATTR_DATABITS,
	    ATTR_STOPBITS,
	    ATTR_PARITY,
	    ATTR_FLOWCTRL,
	    ATTR_DATAMODE,
	    ATTR_RS485DIR,
	};
public:
    SerialPort();
    virtual ~SerialPort();
    virtual bool open(const char* devName,int ioMode);
    virtual bool close();
    virtual int recvData(char * buf, int len, int usTimeout=-1);
    virtual int sendData(const char *buf, int len, int usTimeout=-1);
    virtual int setAttribute(int attr, int value);
    virtual int getAttribute(int attr);
private:
    SerialPortAttr m_attr;
    uint32 m_errorCount{0};
};
}

#endif
