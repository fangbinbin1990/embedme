#ifndef __MEMORY_H__
#define __MEMORY_H__

#include "BaseType.h"
#include "IODevice.h"
#include <iostream>
#include <vector>

namespace libemb{
class MemBlock{
public:
    MemBlock();
    ~MemBlock();
    bool m_isUsed{false};
    void* m_address{NULL};
    std::string m_name{""};
};
class MemoryPool{
public:
    MemoryPool();
    ~MemoryPool();
    bool initPool(int blockNum,int blockSize);
    void* getMemory(const std::string& memoryName,int memorySize);
    bool putMemory(const std::string&  memoryName);
    void showMemory();
private:
    void* m_memory{NULL};
    int m_blockNum{0};
    int m_blockSize{0};
    int m_poolSize{0};
    std::vector<std::unique_ptr<MemBlock>> m_memBlocks;
};

enum MEMSHARED_ATTR_E
{
    MEMSHARED_ATTR_SIZE=0,
};

enum MEMSHARED_TYPE_E
{
    MEMSHARED_TYPE_SHM=0,
    MEMSHARED_TYPE_FILE,
};
/**
 *  \file   Memory.h   
 *  \class  MemShared
 *  \brief  共享内存类
 */
class MemShared:public IODevice{
public:
    MemShared(int type);
    virtual ~MemShared();
    virtual bool open(const char *name=NULL, int ioMode=IO_MODE_INVALID);
    virtual bool close();
    virtual int setAttribute(int attr, int value);
    virtual int getAttribute(int attr);
    void* attach();
    int detach();
    
private:
    int m_shmType{MEMSHARED_TYPE_SHM};
    int m_shmSize{0};
    void* m_shmAddr{NULL};
};
}
#endif
