/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __REGEXP_H__
#define __REGEXP_H__

#include "BaseType.h"
#include <iostream>

/*----------------------------------* 
  RegExp支持的元字符:
  *     匹配0个或多个在*字符前的字符
  .     匹配任意字符
  ^     匹配行首;后面的字符非
  $     匹配行尾
  []    字符集合
  \     转义符
  <>    精确匹配
  {n}   匹配前面的字符出现n次
  {n,}  匹配前面的字符至少出现n次
  {n,m} 匹配前面的字符出现n-m次
  RegExp支持的扩展元字符:
  \w
  
 *----------------------------------*/
namespace libemb{
/**
 *  \file   RegExp.h   
 *  \class  RegExp
 *  \brief  正则表达式类.
 */
class RegExp{
public:
    RegExp();
    ~RegExp();
    bool match(const std::string& pattern,const std::string& source);
    bool match(const std::string& pattern,const std::string& source,std::string& result,int& pos);
    bool match(const std::string& pattern,const std::string& source,StringArray& strArray,IntArray& posArray,int maxMatches=1);
};
}
#endif