/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __COM_UTIL_H__
#define __COM_UTIL_H__

#include "BaseType.h"
#include <iostream>
#include <vector>

namespace libemb{
using namespace std;
/**
 *  \file   ComUtil.h
 *  \class  ComUtil
 *  \brief  工具类
 */
class ComUtil{
public:
	static bool equal(float f1, float f2, float precision);
    static int random(int max);
    static bool isBigEndian(void);
    static uint16 host2LitEndian(uint16 value);
    static uint32 host2LitEndian(uint32 value);
	static float host2LitEndian(float value);
    static uint16 host2BigEndian(uint16 value);
    static uint32 host2BigEndian(uint32 value);
	static float host2BigEndian(float value);
    static string unicodeOneToUtf8String(uint32 unicode);
    static uint32 utf8OneToUnicode(const char* utf8code);	
    static uint16 bitsReverse(uint16 value, uint8 bits);
	static int eval(const char* expression);
};
}
#endif