#ifndef __FS_MACHINE_H__
#define __FS_MACHINE_H__

#include "BaseType.h"
#include <map>
/*********************************************
 *    状态机用法:
 *    1.首先实例化多个状态,在handleState处理各种状态
 *      及控制状态的切换(getStateMachine().setState(XxxSTATE))
 *    2.将状态处理器注册到状态机
 *    3.设置初始状态,启动状态机.
 *    FSMachine stateMachine();
 *    FSHandler* stateHandler = new XxxStateHandler(&stateMachine);
 *    stateMachine.registerState(stateID,stateHandler);
 *    while(1)
 *    {
 *      if(xxx)
 *      {
 *          stateMachine.setState(stateID);
 *      }
 *    }
 *    
 */

namespace libemb{

class FSMachine;
class FSHandler{
public:
    FSHandler(std::shared_ptr<FSMachine> stateMachine);
    virtual ~FSHandler();
    FSMachine& getStateMachine();
    virtual void handleState()=0;
private:
    std::shared_ptr<FSMachine> m_stateMachine;
};

class FSMachine{
public:
    FSMachine();
    virtual ~FSMachine();
    bool registerState(int stateID,std::shared_ptr<FSHandler> stateHandler);
    void setState(int stateID);
    int getState();
private:
    int m_currState{0};
    std::map<int,std::shared_ptr<FSHandler>> m_stateHandlerMap;
};

}

#endif
