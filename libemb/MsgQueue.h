/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __MSGQUEUE_H__
#define __MSGQUEUE_H__

#include "BaseType.h"
#include "Singleton.h"
#include "ThreadUtil.h"

#if defined OS_CYGWIN || defined OS_UNIX
#define USE_MQ_SYSV     (1)     /* cygwin和linux下只能使用sysv的消息队列 */
#else
#define USE_MQ_SYSV     (0)     /* Android下使用posix消息队列 */
#endif
#include <sys/types.h>
#if USE_MQ_SYSV
#include <sys/ipc.h>
#include <sys/msg.h>
#else
#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>
#endif

#include <iostream>
#include <map>

#define MSG_SIZE_MAX    128             /**< 最大消息长度 */
#define POSIX_MQFS      "/dev/mqueue"   /**< Posix消息队列挂载点 */

namespace libemb{
/**
 *  \class  QueueMsgBody
 *  \brief  msgbuf消息结构体
 */
struct QueueMsgBody
{
    int m_msgType;                  /**< 消息类型 */
    unsigned char m_dataLen;        /**< 数据长度 */
    char m_data[MSG_SIZE_MAX-1];    /**< 消息数据 */
};

/**
 *  \file   MsgQueue.h   
 *  \class  MsgQueue
 *  \brief  消息队列类		
 */

class MsgQueue{
public:
	MsgQueue(key_t key);
    virtual ~MsgQueue();
	virtual bool initialize();
    int sendMsg(QueueMsgBody& msg);
    int recvMsg(QueueMsgBody& msg, int msgType=0);
    int clearMsg(int msgType=0);
private:
    std::string getMqName(int key); 
private:
    int m_msgID{-1};
    key_t m_key{-1};
};

/**
 *  \file   MsgQueue.h   
 *  \class  MsgQueueFactory
 *  \brief  消息队列工厂类	
 */

class MsgQueueFactory:public Singleton<MsgQueueFactory>{
DECL_SINGLETON(MsgQueueFactory)
{
}
public:
    ~MsgQueueFactory(){;}
    std::shared_ptr<MsgQueue> getMsgQueue(key_t key);
private:
    Mutex m_mutex;
    std::map<key_t,std::shared_ptr<MsgQueue>> m_msgQueueMap;
}; 
}

#endif

