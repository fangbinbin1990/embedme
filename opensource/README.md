### 此文件夹是开源代码库，可根据项目所需单独编译:
* mbuild_remake opensource/libcjson libcjson
* mbuild_remake opensource/libconfig++ libconfig++
* mbuild_remake opensource/libcurl libcurl
* mbuild_remake opensource/libopencore-amrnb libopencore-amrnb
* mbuild_remake opensource/libsocketcan libsocketcan
* mbuild_remake opensource/libsqlite libsqlite
* mbuild_remake opensource/libtinyalsa libtinyalsa
* mbuild_remake opensource/libtinyxml libtinyxml
* mbuild_remake opensource/libvo-amrwbenc libvo-amrwbenc
* mbuild_remake opensource/libyuv libyuv-x86
