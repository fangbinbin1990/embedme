LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_LIB_TYPE:=static

LOCAL_SUB_DIR:=tinyxml-2.6.2
SUB_MAKEFILE_PATH:=Makefile.mbuild

include $(BUILD_SUBMAKE)