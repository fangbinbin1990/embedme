LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SUB_DIR:=sqlite-3871
SUB_CONFIGURE_SCRIPT:=autoreconf -ivf;automake --add-missing;./configure --prefix=$(LOCAL_OUTPUT_PATH) --host=$(HOST)

include $(BUILD_SUBCONF)