LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SUB_DIR:=vo-amrwbenc-0.1.3
SUB_CONFIGURE_SCRIPT:=./configure --prefix=$(LOCAL_OUTPUT_PATH) --host=$(HOST)

include $(BUILD_SUBCONF)