LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SUB_DIR:=libev-4.27
SUB_CONFIGURE_SCRIPT:=./configure --prefix=$(LOCAL_OUTPUT_PATH) --host=$(HOST)

include $(BUILD_SUBCONF)
