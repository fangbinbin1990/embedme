LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SUB_DIR:=curl-7.67.0
SUB_CONFIGURE_SCRIPT:=./buildconf;./configure --prefix=$(LOCAL_OUTPUT_PATH) --host=$(HOST) --disable-ldap  --disable-ldaps --without-zlib --without-ssl --without-libidn

include $(BUILD_SUBCONF)