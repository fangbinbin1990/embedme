/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __GPIO_H__
#define __GPIO_H__

#include "BaseType.h"
#include "Singleton.h"
#include <map>
namespace libembx{

enum class GpioValue
{
	Low  = 0,
	High = 1,
	Unsure = -1
};
enum class GpioDirection
{
	Input	= 0,
	Output  = 1
};

/**
 *  \file   Gpio.h   
 *  \class  GpioImp
 *  \brief  Gpio实例类
 */
class GpioImp{
public:
	GpioImp(int id);
    ~GpioImp();
    int config(GpioDirection dir, GpioValue value=GpioValue::Low);
    bool setValue(GpioValue value);
    GpioValue getValue();
private:
    int m_id;
    std::string m_gpioDir;
    std::string m_exportFile;
    std::string m_directionFile;
    std::string m_valueFile;
};
/**
 *  \file   GpioFactory.h   
 *  \class  GpioFactory
 *  \brief  Gpio工厂类,负责管理创建和管理Gpio
 */

class GpioFactory:public libemb::Singleton<GpioFactory>{
DECL_SINGLETON(GpioFactory)
{
}
public:
    ~GpioFactory(){};
    std::shared_ptr<GpioImp> getGpioImp(std::string gpioName);
    std::shared_ptr<GpioImp> getGpioImp(std::string group, int pin);
	void clear();
private:
    std::map<int, std::shared_ptr<GpioImp>> m_gpioImpTable;
};
}
#endif
