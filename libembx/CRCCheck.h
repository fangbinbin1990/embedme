/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __CRCCHECK_H__
#define __CRCCHECK_H__

#include "BaseType.h"

namespace libemb{
typedef enum{
    CRC_TYPE_CRC16_8005=0,
    CRC_TYPE_CRC16_1021,
    CRC_TYPE_CRC16_3D65
}CRC_TYPE_E;

/**
 *  \file   CRCCheck.h   
 *  \class  CRCCheck
 *  \brief  CRC校验类.
 */
class CRCCheck{
public:
    CRCCheck();
    ~CRCCheck();
    bool doCRC8Check(const std::string packet, int type,unsigned char& result);
    bool doCRC16Check(const std::string packet, int type,unsigned short& result);
    bool doCRC32Check(const std::string packet, int type,unsigned int& result);
    static bool createCRC16Table(unsigned short poly);
};
}
#endif