/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#include "Event.h"
#include "Tracer.h"

#include <string.h>
#include <unistd.h>
#include <iostream>

#define EVENT_MAX_COUNT		100		/* 事件队列中的容许的最大事件个数 */
namespace libemb{
/**
 *  \brief  Event构造函数
 *  \param  eventID 事件ID
 *  \param  param 事件携带的参数
 *  \param  paramLen 事件携带的参数长度,最长为EVENT_PARAM_MAXLEN字节
 *  \return none
 *  \note   none
 */
Event::Event(int eventID, void* param, int paramLen)
{
	memset(m_eventParam,0,EVENT_PARAM_MAXLEN);
	if((param != NULL) && 
	   (paramLen > 0) && 
	   (paramLen <= EVENT_PARAM_MAXLEN))
	{
		memcpy(m_eventParam, param, paramLen);
		m_eventParamLen = paramLen;
	}
	else
	{
		m_eventParamLen = 0;
	}
	m_eventID = eventID;
}
/**
 *  \brief  Event拷贝构造函数
 *  \param  none
 *  \return none
 *  \note   none
 */
Event::Event(const Event& event)
{
    m_eventID = event.m_eventID;
    m_eventParamLen = event.m_eventParamLen;
    if (m_eventParamLen>0)
    {
        memset(m_eventParam,0,EVENT_PARAM_MAXLEN);
        memcpy(m_eventParam, event.m_eventParam, m_eventParamLen);
    }
}
/**
 *  \brief  Event析构函数
 *  \param  none
 *  \return none
 *  \note   none
 */
Event::~Event()
{
}
/**
 *  \brief  获取事件参数
 *  \param  void
 *  \return 事件ID
 *  \note   none
 */
int Event::getEventId()
{
    return m_eventID;
}
/**
 *  \brief  获取事件参数
 *  \param  buf 参数缓存地址
 *  \param  bufLen 缓存长度
 *  \return 成功返回数据长度,失败返回STATUS_ERROR
 *  \note   none
 */
int Event::getParam(void* buf, int bufLen)
{
	if((NULL==buf) ||
	   (bufLen<=0) ||
	   (bufLen<m_eventParamLen))
	{
		return STATUS_ERROR;
	}
	memcpy(buf,m_eventParam,m_eventParamLen);
	return m_eventParamLen;
}

/**
 *  \brief  EventDispatcher构造函数
 *  \param  none
 *  \return none
 *  \note   要使用事件必须要有EventDispatcher,构造对象是启动事件中心线程
 */
EventDispatcher::EventDispatcher()
{
	m_ecThread.start(this);
}
/**
 *  \brief  EventDispatcher析构函数
 *  \param  none
 *  \return none
 *  \note   none
 */
EventDispatcher::~EventDispatcher()
{
	m_ecThread.cancel();
}

/**
 *  \brief  注册事件监听者
 *  \param  listener 事件监听者
 *  \return void
 *  \note   注册后,事件监听者需要实现handleEvent来处理事件
 */
void EventDispatcher::registerListener(EventListner* listener)
{
	AutoLock lock(&m_listenerLock);
	m_evtListenerList.push_back(listener);
}

/**
 *  \brief  取消注册事件监听者到事件中心
 *  \param  listener 事件监听者
 *  \return void
 *  \note   none
 */
void EventDispatcher::unregisterListener(EventListner* listener)
{
	AutoLock lock(&m_listenerLock);
	m_evtListenerList.remove(listener);
}

/**
 *  \brief  发送事件
 *  \param  evt 事件
 *  \return void
 *  \note   none
 */
void EventDispatcher::postEvent(Event& evt)
{
	AutoLock lock(&m_eventLock);
	m_eventList.push_back(evt);
	TRACE_DBG_CLASS("Post Event:0x%04X\n",evt.getEventId());
}

/* 每个事件分发者通过该线程分发事件给所有注册事件处理者 */
void EventDispatcher::run()
{
	std::list<EventListner*>::iterator listenerIter;
	std::list<Event>::iterator eventIter;
	while(1)
	{
		m_eventLock.lock();
		if(m_eventList.empty())
		{
			m_eventLock.unLock();
			Thread::msleep(10);/* sleep函数不能放在互斥锁中,否则会死锁 */
			continue;
		}
		else
		{	
			m_eventLock.unLock();
			m_listenerLock.lock();
			m_eventLock.lock();
			if (m_evtListenerList.empty())
			{
				if (m_eventList.size()>EVENT_MAX_COUNT)
				{
				    TRACE_WARN_CLASS("EventList Full,Clear...\n");
					m_eventList.clear();
				}
				m_eventLock.unLock();
				m_listenerLock.unLock();
				continue;
			}
			for(eventIter=m_eventList.begin();eventIter!=m_eventList.end();)
			{
				for(listenerIter=m_evtListenerList.begin();listenerIter!=m_evtListenerList.end();listenerIter++)
				{
					(*listenerIter)->handleEvent(*(m_eventList.begin()));
				}
				/* 遍历容器时删除元素时要注意:一定要先获取到下一个元素的位置再删除当前元素!!! */
				m_eventList.erase(eventIter++);
			}
			m_eventLock.unLock();
			m_listenerLock.unLock();
		}
	}
}

/**
 *  \brief  EventCenter构造函数
 *  \param  none
 *  \return none
 */
EventCenter::EventCenter()
{
    m_eventDispatcher = NEW_OBJ EventDispatcher();
}
/**
 *  \brief  EventCenter析构函数
 *  \param  none
 *  \return none
 */
EventCenter::~EventCenter()
{
    DEL_OBJ(m_eventDispatcher);
}

/**
 *  \brief  增加事件监听者
 *  \param  listener 事件处理者
 *  \return void
 *  \note   注册后,事件处理者需要实现handleEvent来处理事件
 */
void EventCenter::addEventListener(EventListner* listener)
{
    m_eventDispatcher->registerListener(listener);
}

/**
 *  \brief  删除事件处理者
 *  \param  listener 事件处理者
 *  \return void
 *  \note   none
 */
void EventCenter::delEventListener(EventListner* listener)
{
	m_eventDispatcher->unregisterListener(listener);
}

/**
 *  \brief  发送事件
 *  \param  evt 事件
 *  \return void
 *  \note   none
 */
void EventCenter::postEvent(Event& evt)
{
	m_eventDispatcher->postEvent(evt);
}
}