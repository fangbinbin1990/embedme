LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := libembx

LOCAL_CFLAGS :=
LOCAL_CXXFLAGS :=
LOCAL_LDFLAGS :=
LOCAL_INC_PATHS := \
	$(LOCAL_PATH) \
	$(PROJECT_ROOT)/libemb

LOCAL_SRC_FILES := \
	CANPort.cpp \
	Config.cpp \
	CppUnitLite.cpp \
	EVIO.cpp \
	Gpio.cpp \
	HttpRequest.cpp \
	JSONData.cpp \
	MD5Check.cpp \
	SuperTester.cpp

LOCAL_LIB_TYPE:=static

include $(BUILD_LIBRARY)