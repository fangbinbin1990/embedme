/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __EVENT_TIMER_H__
#define __EVENT_TIMER_H__

#include "BaseType.h"
#include "DateTime.h"
#include "Event.h"
#include "Thread.h"
#include "ThreadUtil.h"
#include "Timer.h"
#include <list>

namespace libemb{

typedef enum
{
    TIMER_TYPE_PERIODIC,
    TIMER_TYPE_ONESHOT,
}TIMER_TYPE_E;

typedef enum
{
    TIME_UNIT_MILLISECOND,
    TIME_UNIT_SECOND,
    TIME_UNIT_MINUTE,
    TIME_UNIT_HOUR,
    TIME_UNIT_DAY,
}TIME_UNIT_E;

class EventTimerManager;

/**
 *  \file   EventTimer.h   
 *  \class  EventTimer
 *  \brief  定时器基类(基于事件消息的定时器)
 */
class EventTimer{
public:
    EventTimer(DateTime absTime);
    EventTimer(int interval,int unit);
    virtual ~EventTimer();
    int reset();
    virtual void onTimer(EventCenter* eventCenter)=0;
protected:
    void onTick(EventCenter* eventCenter);
protected:
    int m_type;
    int m_event;
private:
    bool m_isAbsTimer;
    DateTime m_absTime;
    int m_timerMS;
    int m_interval;
    int m_unit;
    bool m_isExpired;
friend class EventTimerManager;
};

/**
 *  \file   EventTimer.h   
 *  \class  PeriodicTimer
 *  \brief  周期定时器类	
 */
class PeriodicTimer:public EventTimer{
public:
    PeriodicTimer(DateTime absTime, int event);
    PeriodicTimer(int interval,int unit, int event);
    ~PeriodicTimer();
private:
    void onTimer(EventCenter* eventCenter);
};

/**
 *  \file   EventTimer.h   
 *  \class  OneShotTimer
 *  \brief  一次性定时器类	
 */
class OneShotTimer:public EventTimer{
public:
    OneShotTimer(DateTime absTime, int event);
    OneShotTimer(int interval,int unit, int event);
    ~OneShotTimer();
private:
    void onTimer(EventCenter* eventCenter);
};

/**
 *  \file   EventTimer.h   
 *  \class  EventTimerManager
 *  \brief  定时器管理类	
 */
class EventTimerManager:public Runnable,public EventCenter{
public:
    /**
     *  \brief  获取TimerManager单例
     *  \param  void
     *  \return EventTimerManager* 
     *  \note   none
     */
    static EventTimerManager* getInstance()
    {
        static EventTimerManager instance;
        return &instance;
    }
    ~EventTimerManager();
    int addTimer(EventTimer* timer);      /* 增加定时器 */
    int removeTimer(EventTimer* timer);   /* 移除定时器 */
    int restartTimer(EventTimer* timer);  /* 重新启用定时器 */
private:
    EventTimerManager();
    void run();
private:
    MutexLock m_listLock;
    Thread m_timerThread;
    std::list<EventTimer*> m_timerList;
};
}
#endif