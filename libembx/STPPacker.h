#ifndef __STP_PACKER_H__
#define __STP_PACKER_H__

#include "BaseType.h"
#include "Thread.h"
#include <vector>

namespace libemb{
/**
 *  \file   STPPacker.h   
 *  \class  STPPacker
 *  \brief  一个简单的串口传输协议(Serial Transfer Protocol)封装器.
 *  \note   协议格式:帧标识(FEFE)+帧长度(2Bytes,不大于0x8000,不小于0x0006)+消息内容+校验码(2Bytes)
 *          如果消息内容或校验码中有FEFE,则增加一个FEFE进行转义
 */
class STPPacker{
public:
    STPPacker();
    ~STPPacker();
    std::vector<std::string> unpackOn(const char* buf,int size);
    std::string packData(const char* data,int size);
private:
    std::string getPayload(const std::string& frame);
private:
    std::string m_dataBuffer;
    int m_frameStart;   /* 帧头位置 */
    int m_frameSize;    /* 帧长度 */
    int m_stpState;     /* 解包状态 */
};
}
#endif
