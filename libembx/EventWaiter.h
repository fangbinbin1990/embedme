#ifndef __EVENT_WAITER_H__
#define __EVENT_WAITER_H__

#include "BaseType.h"
#include "Event.h"
#include "EventTimer.h"

namespace libemb{
class EventWaiter : public TimerListener, public EventListner{
public:
    EventWaiter(IntArray eventList,int eventTimeout);
    virtual ~EventWaiter();
    int startWaitTimeout(); 
private:
    void onTimer(int timerId);
    void handleEvent(Event& evt);
private:
	Thread m_thread;
    RTimer* m_timer;
    IntArray m_eventList;
    bool m_hasEvent;
    bool m_isTimeout;
    int m_msTimeout;
    int m_evtWaited;
};
}
#endif
