/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#include "MD5Check.h"
#include "FileUtil.h"
#include "ComUtil.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Rotate a 32 bit integer by n bytes */
#define rol(x,n) (((x)<<(n))|((x)>>(32-(n))))

namespace libembx{
using namespace libemb;
MD5Check::MD5Check()
{
}

MD5Check::~MD5Check()
{
}
/**
 *  \brief  获取文件的MD5值
 *  \param  fileName 文件名
 *  \param  md5sum md5值
 *  \return 成功返回STATUS_OK,失败返回STATUS_ERROR
 *  \note   none
 */
int MD5Check::checkFile(std::string fileName, std::string & md5sum)
{
    MD5Context ctx;
    initContext(&ctx);
    File file;
    uint8 buffer[4096];
    if (!file.open(CSTR(fileName), IO_MODE_RD_ONLY))
    {
        return STATUS_ERROR;
    }
    while (1)
    {
        int n = file.readData((char*)buffer, sizeof(buffer));
        if (STATUS_ERROR==n)
        {
            return STATUS_ERROR;
        }
        else if (0==n)
        {
            break;
        }
        else
        {
            md5Write(&ctx, buffer, n);
        }
    }
    md5Final(&ctx);
    file.close();
    md5sum = "";
    for (auto i = 0; i < 16; i++)
    {
        char tmp[8] = {0};
        sprintf(tmp, "%02x", ctx.buf[i]);
        md5sum += tmp;
    }
    return STATUS_OK;
}
/**
 *  \brief  获取字符串的MD5值
 *  \param  srcString 源字符串
 *  \param  md5sum md5值
 *  \return 成功返回STATUS_OK,失败返回STATUS_ERROR
 *  \note   none
 */
int MD5Check::checkString(std::string srcString,std::string& md5sum)
{
    MD5Context ctx;
    initContext(&ctx);
    uint8* buf=(uint8*)CSTR(srcString);
    if (srcString.empty())
    {
        return STATUS_ERROR;
    }
    md5Write(&ctx, buf, srcString.size());
    md5Final(&ctx);
    md5sum = "";
    for (auto i = 0; i < 16; i++)
    {
        char tmp[8] = {0};
        sprintf(tmp, "%02x", ctx.buf[i]);
        md5sum += tmp;
    }
    return STATUS_OK;
}

void MD5Check::initContext(MD5Context * ctx)
{
    ctx->A = 0x67452301;
    ctx->B = 0xefcdab89;
    ctx->C = 0x98badcfe;
    ctx->D = 0x10325476;
    ctx->nblocks = 0;
    ctx->count = 0;
}

/* transform n*64 bytes */
void MD5Check::transform(MD5Context * ctx, uint8 * data)
{
    uint32 correct_words[16];
    uint32 A = ctx->A;
    uint32 B = ctx->B;
    uint32 C = ctx->C;
    uint32 D = ctx->D;
    uint32 * cwp = correct_words;

    if (ComUtil::isBigEndian())
    {
        int i;
        uint8 * p2, *p1;
        for (i = 0, p1 = data, p2 = (uint8 *)correct_words; i < 16; i++, p2 += 4)
        {
            p2[3] = *p1++;
            p2[2] = *p1++;
            p2[1] = *p1++;
            p2[0] = *p1++;
        }
    }
    else
    {
        memcpy(correct_words, data, 64);
    }

/* These are the four functions used in the four steps of the MD5 algorithm
   and defined in the RFC 1321.  The first function is a little bit optimized
   (as found in Colin Plumbs public domain implementation).  */
/* #define FF(b, c, d) ((b & c) | (~b & d)) */
#define FF(b, c, d) (d ^ (b & (c ^ d)))
#define FG(b, c, d) FF (d, b, c)
#define FH(b, c, d) (b ^ c ^ d)
#define FI(b, c, d) (c ^ (b | ~d))

#define OP(a, b, c, d, s, T)					    \
  do								    \
    {								    \
      a += FF (b, c, d) + (*cwp++) + T; 	    \
      a = rol(a, s);						    \
      a += b;							    \
    }								    \
  while (0)

    /* Before we start, one word about the strange constants.
       They are defined in RFC 1321 as

       T[i] = (int) (4294967296.0 * fabs (sin (i))), i=1..64
     */

    /* Round 1.  */
    OP(A, B, C, D,  7, 0xd76aa478);
    OP(D, A, B, C, 12, 0xe8c7b756);
    OP(C, D, A, B, 17, 0x242070db);
    OP(B, C, D, A, 22, 0xc1bdceee);
    OP(A, B, C, D,  7, 0xf57c0faf);
    OP(D, A, B, C, 12, 0x4787c62a);
    OP(C, D, A, B, 17, 0xa8304613);
    OP(B, C, D, A, 22, 0xfd469501);
    OP(A, B, C, D,  7, 0x698098d8);
    OP(D, A, B, C, 12, 0x8b44f7af);
    OP(C, D, A, B, 17, 0xffff5bb1);
    OP(B, C, D, A, 22, 0x895cd7be);
    OP(A, B, C, D,  7, 0x6b901122);
    OP(D, A, B, C, 12, 0xfd987193);
    OP(C, D, A, B, 17, 0xa679438e);
    OP(B, C, D, A, 22, 0x49b40821);

#undef OP
#define OP(f, a, b, c, d, k, s, T)  \
    do								      \
      { 							      \
	a += f (b, c, d) + correct_words[k] + T;		      \
	a = rol(a, s);						      \
	a += b; 						      \
      } 							      \
    while (0)

    /* Round 2.  */
    OP(FG, A, B, C, D,  1,  5, 0xf61e2562);
    OP(FG, D, A, B, C,  6,  9, 0xc040b340);
    OP(FG, C, D, A, B, 11, 14, 0x265e5a51);
    OP(FG, B, C, D, A,  0, 20, 0xe9b6c7aa);
    OP(FG, A, B, C, D,  5,  5, 0xd62f105d);
    OP(FG, D, A, B, C, 10,  9, 0x02441453);
    OP(FG, C, D, A, B, 15, 14, 0xd8a1e681);
    OP(FG, B, C, D, A,  4, 20, 0xe7d3fbc8);
    OP(FG, A, B, C, D,  9,  5, 0x21e1cde6);
    OP(FG, D, A, B, C, 14,  9, 0xc33707d6);
    OP(FG, C, D, A, B,  3, 14, 0xf4d50d87);
    OP(FG, B, C, D, A,  8, 20, 0x455a14ed);
    OP(FG, A, B, C, D, 13,  5, 0xa9e3e905);
    OP(FG, D, A, B, C,  2,  9, 0xfcefa3f8);
    OP(FG, C, D, A, B,  7, 14, 0x676f02d9);
    OP(FG, B, C, D, A, 12, 20, 0x8d2a4c8a);

    /* Round 3.  */
    OP(FH, A, B, C, D,  5,  4, 0xfffa3942);
    OP(FH, D, A, B, C,  8, 11, 0x8771f681);
    OP(FH, C, D, A, B, 11, 16, 0x6d9d6122);
    OP(FH, B, C, D, A, 14, 23, 0xfde5380c);
    OP(FH, A, B, C, D,  1,  4, 0xa4beea44);
    OP(FH, D, A, B, C,  4, 11, 0x4bdecfa9);
    OP(FH, C, D, A, B,  7, 16, 0xf6bb4b60);
    OP(FH, B, C, D, A, 10, 23, 0xbebfbc70);
    OP(FH, A, B, C, D, 13,  4, 0x289b7ec6);
    OP(FH, D, A, B, C,  0, 11, 0xeaa127fa);
    OP(FH, C, D, A, B,  3, 16, 0xd4ef3085);
    OP(FH, B, C, D, A,  6, 23, 0x04881d05);
    OP(FH, A, B, C, D,  9,  4, 0xd9d4d039);
    OP(FH, D, A, B, C, 12, 11, 0xe6db99e5);
    OP(FH, C, D, A, B, 15, 16, 0x1fa27cf8);
    OP(FH, B, C, D, A,  2, 23, 0xc4ac5665);

    /* Round 4.  */
    OP(FI, A, B, C, D,  0,  6, 0xf4292244);
    OP(FI, D, A, B, C,  7, 10, 0x432aff97);
    OP(FI, C, D, A, B, 14, 15, 0xab9423a7);
    OP(FI, B, C, D, A,  5, 21, 0xfc93a039);
    OP(FI, A, B, C, D, 12,  6, 0x655b59c3);
    OP(FI, D, A, B, C,  3, 10, 0x8f0ccc92);
    OP(FI, C, D, A, B, 10, 15, 0xffeff47d);
    OP(FI, B, C, D, A,  1, 21, 0x85845dd1);
    OP(FI, A, B, C, D,  8,  6, 0x6fa87e4f);
    OP(FI, D, A, B, C, 15, 10, 0xfe2ce6e0);
    OP(FI, C, D, A, B,  6, 15, 0xa3014314);
    OP(FI, B, C, D, A, 13, 21, 0x4e0811a1);
    OP(FI, A, B, C, D,  4,  6, 0xf7537e82);
    OP(FI, D, A, B, C, 11, 10, 0xbd3af235);
    OP(FI, C, D, A, B,  2, 15, 0x2ad7d2bb);
    OP(FI, B, C, D, A,  9, 21, 0xeb86d391);

    /* Put checksum in context given as argument.  */
    ctx->A += A;
    ctx->B += B;
    ctx->C += C;
    ctx->D += D;
}

/* The routine updates the message-digest context to
 * account for the presence of each of the characters inBuf[0..inLen-1]
 * in the message whose digest is being computed.
 */
void MD5Check::md5Write(MD5Context * ctx, uint8 * inbuf, int inlen)
{
    if (ctx->count == 64)    /* flush the buffer */
    {
        transform(ctx, ctx->buf);
        ctx->count = 0;
        ctx->nblocks++;
    }
    if (!inbuf)
    {
        return;
    }
    if (ctx->count)
    {
        for (; inlen && ctx->count < 64; inlen--)
        {
            ctx->buf[ctx->count++] = *inbuf++;
        }
        md5Write(ctx, NULL, 0);
        if (!inlen)
        {
            return;
        }
    }

    while (inlen >= 64)
    {
        transform(ctx, inbuf);
        ctx->count = 0;
        ctx->nblocks++;
        inlen -= 64;
        inbuf += 64;
    }
    for (; inlen && ctx->count < 64; inlen--)
    {
        ctx->buf[ctx->count++] = *inbuf++;
    }
}

/* The routine final terminates the message-digest computation and
 * ends with the desired message digest in mdContext->digest[0...15].
 * The handle is prepared for a new MD5 cycle.
 * Returns 16 bytes representing the digest.
 */
void MD5Check::md5Final(MD5Context * ctx)
{
    uint32 t, msb, lsb;
    uint8 * p;

    md5Write(ctx, NULL, 0); /* flush */;

    t = ctx->nblocks;
    /* multiply by 64 to make a byte count */
    lsb = t << 6;
    msb = t >> 26;
    /* add the count */
    t = lsb;
    if ((lsb += ctx->count) < t)
    {
        msb++;
    }
    /* multiply by 8 to make a bit count */
    t = lsb;
    lsb <<= 3;
    msb <<= 3;
    msb |= t >> 29;

    if (ctx->count < 56)    /* enough room */
    {
        ctx->buf[ctx->count++] = 0x80; /* pad */
        while (ctx->count < 56)
        {
            ctx->buf[ctx->count++] = 0;  /* pad */
        }
    }
    else   /* need one extra block */
    {
        ctx->buf[ctx->count++] = 0x80; /* pad character */
        while (ctx->count < 64)
        {
            ctx->buf[ctx->count++] = 0;
        }
        md5Write(ctx, NULL, 0);  /* flush */;
        memset(ctx->buf, 0, 56);  /* fill next block with zeroes */
    }
    /* append the 64 bit count */
    ctx->buf[56] = lsb	   ;
    ctx->buf[57] = lsb >>  8;
    ctx->buf[58] = lsb >> 16;
    ctx->buf[59] = lsb >> 24;
    ctx->buf[60] = msb	   ;
    ctx->buf[61] = msb >>  8;
    ctx->buf[62] = msb >> 16;
    ctx->buf[63] = msb >> 24;
    transform(ctx, ctx->buf);

    p = ctx->buf;
    if (ComUtil::isBigEndian())
    {
        *p++ = ctx->A;
        *p++ = ctx->A >> 8;
        *p++ = ctx->A >> 16;
        *p++ = ctx->A >> 24;
        *p++ = ctx->B;
        *p++ = ctx->B >> 8;
        *p++ = ctx->B >> 16;
        *p++ = ctx->B >> 24;
        *p++ = ctx->C;
        *p++ = ctx->C >> 8;
        *p++ = ctx->C >> 16;
        *p++ = ctx->C >> 24;
        *p++ = ctx->D;
        *p++ = ctx->D >> 8;
        *p++ = ctx->D >> 16;
        *p++ = ctx->D >> 24;
    }
    else
    {
        *(uint32 *)p = ctx->A ;
        p += 4;
        *(uint32 *)p = ctx->B ;
        p += 4;
        *(uint32 *)p = ctx->C ;
        p += 4;
        *(uint32 *)p = ctx->D ;
        p += 4;
    }
}
}