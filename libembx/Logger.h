/******************************************************************************
 * This file is part of libemb.
 *
 * libemb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libemb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libemb.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: Embedme
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme
 * Copyright 2014~2020 @ ShenZhen ,China
*******************************************************************************/
#ifndef __LOGGER_H__
#define __LOGGER_H__

#include "BaseType.h"
#include "ThreadUtil.h"
#include "Config.h"
#include "SqliteWrapper.h"
#include "Thread.h"
#include "FileUtil.h"

#include <string>
#include <stdio.h>
#include <unistd.h>
#include <stdarg.h>

#include <map>

#define LOG_ON(name,fmt,arg...)     do{Logger* logger=LoggerManager::getInstance()->getLogger(name); \
                                       if(logger!=NULL) logger->log(fmt,##arg); \
                                       else TRACE_ERR("no logger:%s.\n",name); \
                                       TRACE_NONE("LOG@%s>>"fmt,name,##arg);}while(0)

#define LOG_CORE(fmt,arg...)        LOG_ON("core",fmt,##arg)

namespace libemb{
typedef struct{
    std::string m_logName;
    std::string m_logPath;
    int m_logSize;
}LogInfo_S;

/**
 *  \file   Logger.h   
 *  \class  Logger
 *  \brief  日志类(用来存储错误信息日志).
 */
class Logger :public Runnable {
public:
    Logger();
    ~Logger();
    bool init(std::string logPath,std::string logName,int logNumberMax,int logStoreMax, int fileSizeMax);
    void log(const char * fmt, ...);
    bool getLogInfo(LogInfo_S* logInfo);
private:
    void initialize();
    void makeLog(const char * fmt,va_list argp);
    bool updateMaintainFile();
    bool openNewLogFile();
    bool cleanLogFile();
    bool cleanLogStore();
    void run();
private:
    std::string m_logPath;      /* log根目录 */
    std::string m_logName;      /* log名(log根目录下文件夹名称) */
    int m_logNumberMax;         /* 最大日志个数 */
    int m_logStoreMax;          /* 最大可用存储空间 */
    int m_fileSizeMax;          /* 文件最大大小 */
    int m_currentId;
    bool m_initialized; 
    std::vector<std::string> m_logBuffer;
    MutexLock m_logBufferLock;
    File m_logFile;
    Thread m_mainThread;
};

/** 
 *  LoggerManager配置文件,配置信息必须是一个列表:
 *  Config config;
 *	config.initWithFile("logger.cfg");
 *  LoggerManager::getInstance()->initWithSetting(config["LoggerManager"]["appLog"]);
 ------------------------------------------------------------------------------------
LoggerManager:
{
    appLog =(
    {
        logPath="/data/log";  //日志根目录
        logName="core";       //日志文件目录名称
        logNumberMax=10000;   //日志文件最大数目，到达最大值后会循环覆盖
        logStoreMax=1024;     //日志最大可用存储空间(MBytes),到达最大数量后会循环覆盖
        fileSizeMax=1;        //日志文件最大大小
    },
    {
        logPath="/data/log";  //日志根目录
        logName="call";       //日志文件目录名称
        logNumberMax=3000;   //日志文件最大数目，到达最大值后会循环覆盖
        logStoreMax=100;     //日志最大可用存储空间(MBytes),到达最大数量后会循环覆盖
        fileSizeMax=1;        //日志文件最大大小
    });
};
-------------------------------------------------------------------------------------*/
class LoggerManager{
public:
    static LoggerManager* getInstance()
    {
        static LoggerManager instance;
        return &instance;
    }
    ~LoggerManager();
    bool initWithSettings(Settings settings);
    Logger* getLogger(const std::string logName);
    bool exportLog(const std::string dstPath,const std::string logName="");
private:
    LoggerManager();
private:
    typedef std::map<string,Logger*> LoggerMap;
    LoggerMap m_loggerMap;
};
}

#endif

