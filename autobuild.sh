#!/bin/bash
##这是自动编译脚本(autobuild.sh)模板，我们使用自动编译功能时,将此文件放置在工程根目录下
function mbuild_automake()
{
    #mbuild_remake opensource/libsocketcan libsocketcan
    #mbuild_remake opensource/libconfig++ libconfig++
    #mbuild_remake opensource/libev libev
    #mbuild_remake opensource/libcjson libcjson
    #mbuild_remake opensource/libcurl libcurl
    mbuild_remake libemb libemb
    mbuild_remake libembx libembx
    mbuild_remake app/demo demo
}
function mbuild_autoclean()
{
    mbuild_clean libemb libemb
    mbuild_clean libembx libembx
    mbuild_clean app/demo demo
}
